/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Rate;

import java.io.Serializable;

/**
 *
 * @author 65435
 */
public class Rate implements Serializable{
     private int environmentRate;
    private int priceRate;
    private int serviceRate;
    private int foodRate;
    private String customer;

    public Rate() {
    }

    public Rate(int environmentRate, int priceRate, int serviceRate, int foodRate, String customer) {
        this.environmentRate = environmentRate;
        this.priceRate = priceRate;
        this.serviceRate = serviceRate;
        this.foodRate = foodRate;
        this.customer = customer;
    }

    public int getEnvironmentRate() {
        return environmentRate;
    }

    public void setEnvironmentRate(int environmentRate) {
        this.environmentRate = environmentRate;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getPriceRate() {
        return priceRate;
    }

    public void setPriceRate(int priceRate) {
        this.priceRate = priceRate;
    }

    public int getServiceRate() {
        return serviceRate;
    }

    public void setServiceRate(int serviceRate) {
        this.serviceRate = serviceRate;
    }

    public int getFoodRate() {
        return foodRate;
    }

    public void setFoodRate(int foodRate) {
        this.foodRate = foodRate;
    }
    
    
    
    
}
