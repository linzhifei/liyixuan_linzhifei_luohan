/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Rate;

import Bussiness.Rate.Rate;
import java.io.Serializable;

/**
 *
 * @author 65435
 */
public class RestaurantRate extends Rate implements Serializable{
    private int dishRate;

    public int getDishRate() {
        return dishRate;
    }

    public void setDishRate(int dishRate) {
        this.dishRate = dishRate;
    }
    
    
}
