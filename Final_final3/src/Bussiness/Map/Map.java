/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Map;

import Bussiness.LandMark.LandMarkDirectory;
import Userinterface.Customer.CusLocation;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author rohan
 */
public class Map implements Serializable{
    
    private LandMarkDirectory landMarkDir;
    private String mapRouteString;
    private String name;
    
    public Map(){
        landMarkDir = new LandMarkDirectory();
    }

    public String getMapRouteString() {
        return mapRouteString;
    }

    public void setMapRouteString(String mapRouteString) {
        this.mapRouteString = mapRouteString;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LandMarkDirectory getLandCordDir() {
        return landMarkDir;
    }

    public void setLandCordDir(LandMarkDirectory landCordDir) {
        this.landMarkDir = landCordDir;
    }
    
//    public LandMark addCord(int x, int y){
//        CusLocation cus = new CusLocation();
//        cus.setX(x);
//        cus.setY(y);
//        return cus;
//    }
    
    
}
