/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Organization;

import Bussiness.Role.KitchenRole;
import Bussiness.Role.Role;
import Bussiness.Role.TicketCheckRole;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class TicketCheckOrganization extends Organization implements Serializable{

    public TicketCheckOrganization() {
         super(Organization.Type.TicketCheck.getValue());
        
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new TicketCheckRole());
        return roles;
    } 
    
}
