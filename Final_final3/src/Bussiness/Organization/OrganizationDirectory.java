/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Organization;

import Bussiness.Enterprise.RestaurantEnterprise;
import Bussiness.Organization.Organization.Type;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public class OrganizationDirectory implements Serializable{
        private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.Clean.getValue())){
            organization = new CleanOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Feedback.getValue())){
            organization = new FeedbackOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.Kitchen.getValue())){
            organization = new KitchenOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.TicketCheck.getValue())){
            organization = new TicketCheckOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.FrontDesk.getValue())){
            organization = new FrontDeskOrganization();
            organizationList.add(organization);
        }else if(type.getValue().equals(Type.Waiter.getValue())){
            organization = new WaiterOrganization();
            organizationList.add(organization);
        }
        
        return organization;
    }
}
