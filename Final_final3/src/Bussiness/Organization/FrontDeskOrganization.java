/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Organization;

import Bussiness.Role.FrontDeskRole;
import Bussiness.Role.KitchenRole;
import Bussiness.Role.Role;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class FrontDeskOrganization extends Organization implements Serializable{

    public FrontDeskOrganization() {
          super(Organization.Type.FrontDesk.getValue());
        
    }

    
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new FrontDeskRole());
        return roles;
    } 
    
}
