/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Organization;

import Bussiness.Role.CleanRole;
import Bussiness.Role.KitchenRole;
import Bussiness.Role.Role;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class CleanOrganization extends Organization implements Serializable{

    public CleanOrganization() {
      super(Organization.Type.Clean.getValue());
        
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new CleanRole());
        return roles;
    } 
    
}
