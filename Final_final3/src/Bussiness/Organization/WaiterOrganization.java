/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Organization;

import Bussiness.Role.KitchenRole;
import Bussiness.Role.Role;
import Bussiness.Role.WaiterRole;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class WaiterOrganization extends Organization implements Serializable{

    public WaiterOrganization() {
        super(Organization.Type.Waiter.getValue());
        
    }

    
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new WaiterRole());
        return roles;
    } 
    
    
}
