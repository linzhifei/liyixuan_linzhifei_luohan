/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Employee;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public class EmployeeCatalog implements Serializable{
    
    private ArrayList<Employee> employeeList;

    public EmployeeCatalog() {
        employeeList = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployeeList() {
        return employeeList;
    }
    
    public Employee createEmployee(String name){
        Employee employee = new Employee();
        employee.setName(name);
        employeeList.add(employee);
        return employee;
    }
    
}
