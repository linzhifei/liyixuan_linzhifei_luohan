/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.LandMark;

import Userinterface.Customer.CusLocation;
import java.io.Serializable;

/**
 *
 * @author rohan
 */
public class LandMark implements Serializable{
    
    private CusLocation cusLocation;
    private String name = null;
    private boolean avaliability;

    public LandMark(){
        cusLocation = new CusLocation();
    }

    public String getName() {
        return name;
    }

    public void setName(String id) {
        this.name = id;
    }

    public boolean isAvaliability() {
        return avaliability;
    }

    public void setAvaliability(boolean avaliability) {
        this.avaliability = avaliability;
    }

    public CusLocation getCusLocation() {
        return cusLocation;
    }

    public void setCusLocation(CusLocation cusLocation) {
        this.cusLocation = cusLocation;
    }
    
    @Override
    public String toString(){
        return name;
    }
    
}
