/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.LandMark;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author rohan
 */
public class LandMarkDirectory implements Serializable{
    
    private ArrayList<LandMark> landMarkDir;
    
    public LandMarkDirectory(){
        landMarkDir = new ArrayList<>();
    }

    public ArrayList<LandMark> getLandMarkDir() {
        return landMarkDir;
    }

    public void setLandMarkDir(ArrayList<LandMark> landMarkDir) {
        this.landMarkDir = landMarkDir;
    }

    public LandMark addAndCreateLandMark(int x, int y, String name, boolean avail){
        LandMark lm = new LandMark();
        lm.getCusLocation().setX(x);
        lm.getCusLocation().setY(y);
        lm.setName(name);
        lm.setAvaliability(avail);
        landMarkDir.add(lm);
        return lm;
    }
    
}
