/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Product;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author redbeanlyx
 */
public class ProductDirectory implements Serializable{
    private ArrayList<Product> productDirectory;

    public ProductDirectory() {
        this.productDirectory = new ArrayList<>();
    }

    public ArrayList<Product> getProductDirectory() {
        return productDirectory;
    }

    public void setProductDirectory(ArrayList<Product> productDirectory) {
        this.productDirectory = productDirectory;
    }
    
    public Product createProduct(String name, String path, int price){
        Product p = new Product();
        p.setImagePath(path);
        p.setName(name);
        p.setProductPrice(price);
        this.productDirectory.add(p);
        return p;
    }
    
}
