/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Role;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.FrontDeskOrganization;
import Bussiness.Organization.Organization;
import Bussiness.Organization.WaiterOrganization;
import Bussiness.UserAccount.UserAccount;
import Userinterface.FrontDesk.FrontDeskWorkAreaJPanel;
import Userinterface.Waiter.WaiterWorkAreaJPanel;
import java.io.Serializable;
import javax.swing.JPanel;

/**
 *
 * @author 65435
 */
public class WaiterRole extends Role implements Serializable{
     @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new WaiterWorkAreaJPanel(userProcessContainer,account,(WaiterOrganization)organization, enterprise,business);
    }
}
