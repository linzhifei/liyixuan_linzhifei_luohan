/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Customer;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class CustomerDirectory implements Serializable{
    private ArrayList<Customer> customerList;

    public CustomerDirectory() {
        customerList = new ArrayList<>();
    }

    public ArrayList<Customer> getEmployeeList() {
        return customerList;
    }
    
    public Customer createCustomer(String name){
        Customer c = new Customer();
        c.setName(name);
       customerList.add(c);
        return c;
    }
}
