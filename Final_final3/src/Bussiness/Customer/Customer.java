/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Customer;

import java.io.Serializable;

/**
 *
 * @author 65435
 */
public class Customer implements Serializable{
    private String name;
    private int id;
    private static int count = 1;
    private String path;

    public Customer() {
        id = count;
        count++;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    public String getName() {
        return name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }  

    @Override
    public String toString() {
        return name;
    }
}
