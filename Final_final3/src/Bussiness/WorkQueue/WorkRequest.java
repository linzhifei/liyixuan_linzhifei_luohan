/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.WorkQueue;

import Bussiness.Customer.Customer;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.Organization;
import Bussiness.Product.Product;
import Bussiness.Rate.Rate;
import Bussiness.UserAccount.UserAccount;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author zinanwang
 */
public class WorkRequest implements Serializable{

    private String customer;
    private Enterprise enterprise;
    private UserAccount sender;
    private UserAccount receiver;
    private UserAccount implementer;
    private String status;
    private Date startTime;
    private Date implementTime;
    private int Price;
    private String promoCode;
    private int id;
    private static int count = 1;
    private Product productName;
    private Rate rate;
    private boolean urgent;

    public WorkRequest() {
        id = count;
        count++;
        this.urgent=false;
        this.receiver = new UserAccount();
        this.sender = new UserAccount();
        this.implementer=new UserAccount();
        this.rate=new Rate();
        this.productName=new Product();
    }

    public Rate getRate() {
        return rate;
    }

    public void setRate(Rate rate) {
        this.rate = rate;
    }

    public static int getCount() {
        return count;
    }

    public static void setCount(int count) {
        WorkRequest.count = count;
    }

    public boolean isUrgent() {
        return urgent;
    }

    public void setUrgent(boolean urgent) {
        this.urgent = urgent;
    }

   
    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public UserAccount getImplementer() {
        return implementer;
    }

    public void setImplementer(UserAccount implementer) {
        this.implementer = implementer;
    }

    public Product getProductName() {
        return productName;
    }

    public void setProductName(Product productName) {
        this.productName = productName;
    }

    
    public Date getImplementTime() {
        return implementTime;
    }

    public void setImplementTime(Date implementTime) {
        this.implementTime = implementTime;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int Price) {
        this.Price = Price;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    public String toString(){
        return String.valueOf(this.getId());
    }
    
    
}
