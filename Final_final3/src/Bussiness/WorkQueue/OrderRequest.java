/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.WorkQueue;

import Bussiness.Enterprise.Enterprise;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author zinanwang
 */
public class OrderRequest extends WorkRequest implements Serializable{

    private String testResult;
    private String ename;
    private Date endTime;
   

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

  

}
