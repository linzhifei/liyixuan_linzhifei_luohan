/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Enterprise;

import Bussiness.UserAccount.UserAccount;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public class EnterpriseDirectory implements Serializable{
    private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }
    
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.Cinema){
            enterprise = new CinemaEnterprise(name);
            enterpriseList.add(enterprise);
        }else if(type == Enterprise.EnterpriseType.Restaurant){
            enterprise = new RestaurantEnterprise(name);
            enterpriseList.add(enterprise);
        }else if(type == Enterprise.EnterpriseType.Hotel){
            enterprise = new HotelEnterprise(name);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }
    
    
    public boolean checkIfEnterpriseIsUnique(String username){
        for (Enterprise e: enterpriseList){
            if (e.getName().equals(username))
                return false;
        }
        return true;
    }
}
