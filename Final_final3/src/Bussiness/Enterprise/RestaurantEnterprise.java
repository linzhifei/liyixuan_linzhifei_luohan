/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Enterprise;

import Bussiness.Role.Role;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public class RestaurantEnterprise extends Enterprise implements Serializable{
    
  private int dishRate;
  
    public RestaurantEnterprise(String name){
        super(name, EnterpriseType.Restaurant);
        
    }

    public int getDishRate() {
        return dishRate;
    }

    public void setDishRate(int dishRate) {
        this.dishRate = dishRate;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole(){
        return null;
    }
    
    
    public int CalculateDish(int timeRate, int tastyRate){
        dishRate=(timeRate+tastyRate)/2;
        return dishRate;
    }
}
