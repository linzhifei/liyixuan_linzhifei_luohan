/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.AdminWorkArea;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Product.Product;
import Bussiness.UserAccount.UserAccount;
import Bussiness.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import java.io.File;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author redbeanlyx
 */
public class ManageMenuJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageMenuJPanel
     */
    private EcoSystem system;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private JPanel userProcessContainer;
    private File file;
    private String picpath;
    
    public ManageMenuJPanel(JPanel userProcessContainer, Enterprise enterprise) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        
       
        populateMenu();
        
    }

    public void populateMenu() {
        
        ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        String picturePath1 = spa.get(0).getImagePath();
        String picturePath2 = spa.get(1).getImagePath();
        String picturePath3 = spa.get(2).getImagePath();
        String picturePath4 = spa.get(3).getImagePath();
        String picturePath5 = spa.get(4).getImagePath();
        String picturePath6 = spa.get(5).getImagePath();
        
        ImageIcon img1 = new ImageIcon(picturePath1);
        ImageIcon img2 = new ImageIcon(picturePath2);
        ImageIcon img3 = new ImageIcon(picturePath3);
        ImageIcon img4 = new ImageIcon(picturePath4);
        ImageIcon img5 = new ImageIcon(picturePath5);
        ImageIcon img6 = new ImageIcon(picturePath6);
        System.out.println(img1);
        img1.setImage(img1.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        img2.setImage(img2.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        img3.setImage(img3.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        img4.setImage(img4.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        img5.setImage(img5.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        img6.setImage(img6.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        
        picture1.setIcon(img1);
        picture2.setIcon(img2);
        picture3.setIcon(img3);
        picture4.setIcon(img4);
        picture5.setIcon(img5);
        picture6.setIcon(img6);
        
        product1.setText(spa.get(0).getName());
        product2.setText(spa.get(1).getName());
        product3.setText(spa.get(2).getName());
        product4.setText(spa.get(3).getName());
        product5.setText(spa.get(4).getName());
        product6.setText(spa.get(5).getName());
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        picture5 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        picture6 = new javax.swing.JLabel();
        picture4 = new javax.swing.JLabel();
        picture3 = new javax.swing.JLabel();
        picture1 = new javax.swing.JLabel();
        picture2 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        product2 = new javax.swing.JTextField();
        product1 = new javax.swing.JTextField();
        product3 = new javax.swing.JTextField();
        product4 = new javax.swing.JTextField();
        product5 = new javax.swing.JTextField();
        product6 = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();

        picture5.setText("null");

        jButton1.setText("<<Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        picture6.setText("null");

        picture4.setText("null");

        picture3.setText("null");

        picture1.setText("null");

        picture2.setText("null");

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 3, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Menu");

        jButton2.setText("Save");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("Change Photo");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("Change Photo");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setText("Change Photo");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setText("Change Photo");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton7.setText("Change Photo");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jButton8.setText("Change Photo");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(152, 152, 152)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(picture5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(product1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(product3, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(product5, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(187, 187, 187)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(product6, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(product4, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(product2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButton7)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 176, Short.MAX_VALUE)
                                        .addComponent(jButton8))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButton5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton6))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jButton3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jButton4)))
                                .addGap(64, 64, 64))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(179, 179, 179))))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap(298, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(77, 77, 77))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(picture4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture6, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGap(186, 186, 186)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(346, 346, 346)
                                .addComponent(product3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(123, 123, 123))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton3)
                                        .addComponent(jButton4))
                                    .addComponent(picture1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(product1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(31, 31, 31)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(picture3, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton5)
                                        .addComponent(jButton6)))
                                .addGap(50, 50, 50)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(picture5, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jButton7)
                                        .addComponent(jButton8)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addComponent(product5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(product2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(150, 150, 150)
                        .addComponent(product4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(123, 123, 123)
                        .addComponent(product6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(33, 33, 33)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(171, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(34, 34, 34)
                    .addComponent(picture2, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(60, 60, 60)
                    .addComponent(picture4, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(50, 50, 50)
                    .addComponent(picture6, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(285, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        spa.get(0).setName(product1.getText());
        spa.get(1).setName(product2.getText());
        spa.get(2).setName(product3.getText());
        spa.get(3).setName(product4.getText());
        spa.get(4).setName(product5.getText());
        spa.get(5).setName(product6.getText());
        
        populateMenu();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.showDialog(new JLabel(), "Choose");
        file = jfc.getSelectedFile();
        if (file.isFile()) {
            System.out.println("Path:" + file.getAbsolutePath());
            
            String path = null;
            
            path = file.getAbsolutePath();
            if (path.endsWith(".jpg")) {
                ImageIcon img = new ImageIcon(path);

                //   icon.setImage(obj.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT));
                img.setImage(img.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                
                picture1.setIcon(img);
                
                picpath = file.getAbsolutePath();
            }
            spa.get(0).setImagePath(path);
        } else {
            
            JOptionPane.showMessageDialog(null, "It's not a file.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
         ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.showDialog(new JLabel(), "Choose");
        file = jfc.getSelectedFile();
        if (file.isFile()) {
            System.out.println("Path:" + file.getAbsolutePath());
            
            String path = null;
            
            path = file.getAbsolutePath();
            if (path.endsWith(".jpg")) {
                ImageIcon img = new ImageIcon(path);

                //   icon.setImage(obj.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT));
                img.setImage(img.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                
                picture3.setIcon(img);
                
                picpath = file.getAbsolutePath();
            }
            spa.get(0).setImagePath(path);
        } else {
            
            JOptionPane.showMessageDialog(null, "It's not a file.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
         ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.showDialog(new JLabel(), "Choose");
        file = jfc.getSelectedFile();
        if (file.isFile()) {
            System.out.println("Path:" + file.getAbsolutePath());
            
            String path = null;
            
            path = file.getAbsolutePath();
            if (path.endsWith(".jpg")) {
                ImageIcon img = new ImageIcon(path);

                //   icon.setImage(obj.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT));
                img.setImage(img.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                
                picture2.setIcon(img);
                
                picpath = file.getAbsolutePath();
            }
            spa.get(0).setImagePath(path);
        } else {
            
            JOptionPane.showMessageDialog(null, "It's not a file.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
         ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.showDialog(new JLabel(), "Choose");
        file = jfc.getSelectedFile();
        if (file.isFile()) {
            System.out.println("Path:" + file.getAbsolutePath());
            
            String path = null;
            
            path = file.getAbsolutePath();
            if (path.endsWith(".jpg")) {
                ImageIcon img = new ImageIcon(path);

                //   icon.setImage(obj.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT));
                img.setImage(img.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                
                picture4.setIcon(img);
                
                picpath = file.getAbsolutePath();
            }
            spa.get(0).setImagePath(path);
        } else {
            
            JOptionPane.showMessageDialog(null, "It's not a file.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        // TODO add your handling code here:
         ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.showDialog(new JLabel(), "Choose");
        file = jfc.getSelectedFile();
        if (file.isFile()) {
            System.out.println("Path:" + file.getAbsolutePath());
            
            String path = null;
            
            path = file.getAbsolutePath();
            if (path.endsWith(".jpg")) {
                ImageIcon img = new ImageIcon(path);

                //   icon.setImage(obj.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT));
                img.setImage(img.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                
                picture5.setIcon(img);
                
                picpath = file.getAbsolutePath();
            }
            spa.get(0).setImagePath(path);
        } else {
            
            JOptionPane.showMessageDialog(null, "It's not a file.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
         ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        JFileChooser jfc = new JFileChooser();
        jfc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        jfc.showDialog(new JLabel(), "Choose");
        file = jfc.getSelectedFile();
        if (file.isFile()) {
            System.out.println("Path:" + file.getAbsolutePath());
            
            String path = null;
            
            path = file.getAbsolutePath();
            if (path.endsWith(".jpg")) {
                ImageIcon img = new ImageIcon(path);

                //   icon.setImage(obj.getImage().getScaledInstance(width, height,Image.SCALE_DEFAULT));
                img.setImage(img.getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
                
                picture6.setIcon(img);
                
                picpath = file.getAbsolutePath();
            }
            spa.get(0).setImagePath(path);
        } else {
            
            JOptionPane.showMessageDialog(null, "It's not a file.", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
         // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel picture1;
    private javax.swing.JLabel picture2;
    private javax.swing.JLabel picture3;
    private javax.swing.JLabel picture4;
    private javax.swing.JLabel picture5;
    private javax.swing.JLabel picture6;
    private javax.swing.JTextField product1;
    private javax.swing.JTextField product2;
    private javax.swing.JTextField product3;
    private javax.swing.JTextField product4;
    private javax.swing.JTextField product5;
    private javax.swing.JTextField product6;
    // End of variables declaration//GEN-END:variables
}
