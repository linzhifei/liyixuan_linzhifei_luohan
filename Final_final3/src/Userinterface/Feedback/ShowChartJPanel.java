/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.Feedback;

import Bussiness.Enterprise.Enterprise;
import Bussiness.Rate.Rate;
import Userinterface.SignUpJPanel;
import java.awt.CardLayout;
import java.awt.Font;
import java.util.Iterator;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.ValueAxis;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author redbeanlyx
 */
public class ShowChartJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ShowChartJPanel
     */
    private ChartPanel frame1;
    private JPanel userProcessContainer;
    private Enterprise enterprise;

    public ShowChartJPanel(JPanel userProcessContainer, Enterprise enterprise) {

        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;

        CategoryDataset dataset = getDataSet();
        JFreeChart chart = ChartFactory.createBarChart3D(
                "Customer Rating Statistics", // 图表标题  
                "number of star", // 目录轴的显示标签  
                "number of customer", // 数值轴的显示标签  
                dataset, // 数据集  
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直  
                true, // 是否显示图例(对于简单的柱状图必须是false)  
                false, // 是否生成工具  
                false // 是否生成URL链接  
        );

        CategoryPlot plot = chart.getCategoryPlot();//获取图表区域对象  
//        org.jfree.chart.axis.CategoryAxis domainAxis = plot.getDomainAxis();         //水平底部列表  
//        domainAxis.setLabelFont(new Font("黑体", Font.BOLD, 14));         //水平底部标题  
//        domainAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 12));  //垂直标题  
//        org.jfree.chart.axis.ValueAxis rangeAxis = plot.getRangeAxis();//获取柱状  
//        rangeAxis.setLabelFont(new Font("黑体", Font.BOLD, 15));
//        chart.getLegend().setItemFont(new Font("黑体", Font.BOLD, 15));
//        chart.getTitle().setFont(new Font("宋体", Font.BOLD, 20));//设置标题字体  

        //到这里结束，虽然代码有点多，但只为一个目的，解决汉字乱码问题  
        frame1 = new ChartPanel(chart, true);  //这里也可以用chartFrame,可以直接生成一个独立的Frame  
        // userProcessContainer.add("frame", frame1);

        jSplitPane1.setBottomComponent(frame1);
    }

       private CategoryDataset getDataSet() {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        int price1 = 0;
        int price2 = 0;
        int price3 = 0;
        int price4 = 0;
        int price5 = 0;

        int en1 = 0;
        int en2 = 0;
        int en3 = 0;
        int en4 = 0;
        int en5 = 0;

        int service1 = 0;
        int service2 = 0;
        int service3 = 0;
        int service4 = 0;
        int service5 = 0;

        int averagePrice = 0;
        int averageEnviroment = 0;
        int averageService = 0;
       

        for (Rate r : enterprise.getRateList().getRateList()) {

            if (r.getPriceRate() == 1) {
                price1++;
            } else if (r.getPriceRate() == 2) {
                price2++;
            } else if (r.getPriceRate() == 3) {
                price3++;
            } else if (r.getPriceRate() == 4) {
                price4++;
            } else {
                price5++;
            }

            if (r.getEnvironmentRate() == 1) {
                en1++;
            } else if (r.getEnvironmentRate() == 2) {
                en2++;
            } else if (r.getEnvironmentRate() == 3) {
                en3++;
            } else if (r.getEnvironmentRate() == 4) {
                en4++;
            } else {
                en5++;
            }

            if (r.getServiceRate() == 1) {
                service1++;
            } else if (r.getServiceRate() == 2) {
                service2++;
            } else if (r.getServiceRate() == 3) {
                service3++;
            } else if (r.getServiceRate() == 4) {
                service4++;
            } else {
                service5++;
            }

        }

        averagePrice = (price1 * 1 + price2 * 2 + price3 * 3 + price4 * 4 + price5 * 5) / (price1 + price2 + price3 + price4 + price5);
        averageEnviroment = (en1 * 1 + en2 * 2 + en3 * 3 + en4 * 4 + en5 * 5) / (en1 + en2 + en3 + en4 + en5);
        averageService = (service1 * 1 + service2 * 2 + service3 * 3 + service4 * 4 + service5 * 5) / (service1 + service2 + service3 + service4 + service5);
     
        
        txtPrice.setText(String.valueOf(averagePrice));
        txtEn.setText(String.valueOf(averageEnviroment));
        txtService.setText(String.valueOf(averageService));
       
        dataset.addValue(price1, "price", "1");
        dataset.addValue(en1, "enviroment", "1");
        dataset.addValue(service1, "service", "1");
        dataset.addValue(price2, "price", "2");
        dataset.addValue(en2, "enviroment", "2");
        dataset.addValue(service2, "service", "2");
        dataset.addValue(price3, "price", "3");
        dataset.addValue(en3, "enviroment", "3");
        dataset.addValue(service3, "service", "3");
        dataset.addValue(price4, "price", "4");
        dataset.addValue(en4, "enviroment", "4");
        dataset.addValue(service4, "service", "4");
        dataset.addValue(price5, "price", "5");
        dataset.addValue(en5, "enviroment", "5");
        dataset.addValue(service5, "service", "5");

        return dataset;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jButton3 = new javax.swing.JButton();
        txtPrice = new javax.swing.JTextField();
        txtEn = new javax.swing.JTextField();
        txtService = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();

        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);

        jLabel1.setText("average Enviroment rating:");

        jLabel2.setText("average Service rating:");

        jLabel3.setText("average price rating:");

        jButton3.setText("<< Back");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        txtPrice.setEnabled(false);
        txtPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceActionPerformed(evt);
            }
        });

        txtEn.setEnabled(false);

        txtService.setEnabled(false);

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel4.setText("Customer rating statistics");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addGap(75, 75, 75)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtEn, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtService, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(171, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txtEn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel1))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtService, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jLabel2))))
                    .addComponent(jButton3))
                .addContainerGap())
        );

        jSplitPane1.setTopComponent(jPanel1);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 636, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 341, Short.MAX_VALUE)
        );

        jSplitPane1.setRightComponent(jPanel2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jSplitPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 377, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txtPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPriceActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTextField txtEn;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtService;
    // End of variables declaration//GEN-END:variables
}
