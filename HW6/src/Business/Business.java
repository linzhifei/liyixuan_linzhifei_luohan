/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *
 * @author 65435
 */
public class Business {

    private MarketOfferList marketOfferList;
    private MarketList marketList;
    private SupplierList supplierList;
    private SalesPersonList salesPersonList;
    private PersonList personList;

    public Business() {
        this.supplierList = new SupplierList();
        this.marketList = new MarketList();
        this.personList = new PersonList();
        this.salesPersonList = new SalesPersonList();
        this.marketOfferList = new MarketOfferList();
    }

    public SupplierList getSupplierList() {
        return supplierList;
    }

    public void setSupplierList(SupplierList SupplierList) {
        this.supplierList = SupplierList;
    }

    public MarketOfferList getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(MarketOfferList marketOfferCatalog) {
        this.marketOfferList = marketOfferCatalog;
    }

    public MarketList getMarketList() {
        return marketList;
    }

    public void setMarketList(MarketList marketList) {
        this.marketList = marketList;
    }

    public SalesPersonList getSalesPersonList() {
        return salesPersonList;
    }

    public void setSalesPersonList(SalesPersonList salesPersonList) {
        this.salesPersonList = salesPersonList;
    }

    public PersonList getPersonList() {
        return personList;
    }

    public void setPersonList(PersonList personList) {
        this.personList = personList;
    }

    //Top 10 sales persons by revenues broken down by market
    public ArrayList<SalesPerson> bestSales() {
        ArrayList<SalesPerson> bestSales = new ArrayList<SalesPerson>();
        ArrayList<SalesPersonRevenue> srList = new ArrayList<SalesPersonRevenue>();
        //calculate revenue for each saleaman
        for (SalesPerson sp : salesPersonList.getSalePersonList()) {
            double re = 0;
            for (Order o : sp.getOrderList()) {
                for (OrderItem oi : o.getOrderItemList()) {
                    re += (oi.getSalesPrice() - oi.getMarketOffer().getTarget());
                }
            }
            SalesPersonRevenue sr = new SalesPersonRevenue();
            sr.setRevenue(re);
            sr.setSalesPerson(sp);
            srList.add(sr);
        }
        //sort
        Collections.sort(srList, new SortByRe());
        for (SalesPersonRevenue sr : srList) {
            System.out.println(sr.getSalesPerson().getName() + "  " + sr.getRevenue());
        }
        //put into bestlist
        if (srList.size() < 10) {
            for(int i=0;i<srList.size();i++){
                bestSales.add(srList.get(i).getSalesPerson());
            }

        } else {
            bestSales.add(srList.get(0).getSalesPerson());
            bestSales.add(srList.get(1).getSalesPerson());
            bestSales.add(srList.get(2).getSalesPerson());
            bestSales.add(srList.get(3).getSalesPerson());
            bestSales.add(srList.get(4).getSalesPerson());
            bestSales.add(srList.get(5).getSalesPerson());
            bestSales.add(srList.get(6).getSalesPerson());
            bestSales.add(srList.get(7).getSalesPerson());
            bestSales.add(srList.get(8).getSalesPerson());
            bestSales.add(srList.get(9).getSalesPerson());
        }
        return bestSales;
    }

    class SortByRe implements Comparator {

        public int compare(Object o1, Object o2) {
            ProductRevenue pr1 = (ProductRevenue) o1;
            ProductRevenue pr2 = (ProductRevenue) o2;
            if (pr1.getRevenue() > pr2.getRevenue()) {
                return 1;
            }
            return 0;
        }
    }

    //Top 3 products consistently sold above market target price
    public ArrayList<Product> bestProduct() {
        ArrayList<Product> bestProduct = new ArrayList<Product>();
        ArrayList<Product> productList = new ArrayList<Product>();
        ArrayList<ProductRevenue> prList = new ArrayList<ProductRevenue>();
        for (SalesPerson sp : salesPersonList.getSalePersonList()) {
            for (Order order : sp.getOrderList()) {
                for (OrderItem oi : order.getOrderItemList()) {
                    if (!productList.contains(oi.getMarketOffer().getProduct())) {
                        productList.add(oi.getMarketOffer().getProduct());
                    }
                }
            }
        }
        //calculate revenue for each product
        for (Product p : productList) {
            for (SalesPerson sp : salesPersonList.getSalePersonList()) {
                double re = 0;
                for (Order order : sp.getOrderList()) {
                    for (OrderItem oi : order.getOrderItemList()) {
                        if (p.getName().equals(oi.getMarketOffer().getProduct().getName())) {
                            re += (oi.getSalesPrice() - oi.getMarketOffer().getTarget());
                        }
                    }
                }
                ProductRevenue pr = new ProductRevenue();
                pr.setProduct(p);
                pr.setRevenue(re);
                prList.add(pr);
            }
        }
        //sort
        Collections.sort(prList, new SortByRe());
        for (ProductRevenue pr : prList) {
            System.out.println(pr.getProduct().getName() + "  " + pr.getRevenue());
        }
        //put into bestlist
        bestProduct.add(prList.get(0).getProduct());
        bestProduct.add(prList.get(1).getProduct());
        bestProduct.add(prList.get(2).getProduct());
        return bestProduct;
    }

    //Product sales revenues by market
    public int marketRevenue(String marketType) {
        int marketRevenue = 0;
        for (SalesPerson sp : salesPersonList.getSalePersonList()) {
            for (Order order : sp.getOrderList()) {
                for (OrderItem oi : order.getOrderItemList()) {
                    if (oi.getMarketOffer().getMarket().getMarketType().equals(marketType)) {
                        marketRevenue += (oi.getSalesPrice() - oi.getMarketOffer().getTarget());
                    }
                }
            }
        }
        return marketRevenue;
    }

//Revenue totals for Xerox
    public int totalRevenue() {
        int total = 0;
        for (SalesPerson sp : salesPersonList.getSalePersonList()) {
            for (Order order : sp.getOrderList()) {
                for (OrderItem oi : order.getOrderItemList()) {
                    total += (oi.getSalesPrice() - oi.getMarketOffer().getTarget());
                }
            }
        }
        return total;
    }

//Sales persons with consist above target sales
    public ArrayList<SalesPerson> aboveTarget() {
        ArrayList<SalesPerson> aboveSales = new ArrayList<SalesPerson>();
        for (SalesPerson sp : salesPersonList.getSalePersonList()) {
            int personRevenue = 0;
            for (Order order : sp.getOrderList()) {
                for (OrderItem oi : order.getOrderItemList()) {
                    personRevenue += (oi.getSalesPrice() - oi.getMarketOffer().getTarget());
                }
            }
            if (personRevenue > 0) {
                aboveSales.add(sp);
            }
        }
        return aboveSales;
    }

    //   Sales person with below total order target sales
    public ArrayList<SalesPerson> belowTraget() {
        ArrayList<SalesPerson> belowSales = new ArrayList<SalesPerson>();
        for (SalesPerson sp : salesPersonList.getSalePersonList()) {
            int personRevenue = 0;
            for (Order order : sp.getOrderList()) {
                for (OrderItem oi : order.getOrderItemList()) {
                    personRevenue += (oi.getSalesPrice() - oi.getMarketOffer().getTarget());
                }
            }
            if (personRevenue < 0) {
                belowSales.add(sp);
            }
        }
        return belowSales;
    }

}
