/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class SalesPerson extends Person{
    
    private ArrayList<Order> orderList;

    public SalesPerson(){
        orderList = new ArrayList<Order>();
    }
    
    public ArrayList<Order> getOrderList() {
        return orderList;
    }

    public void setOrderList(ArrayList<Order> orderList) {
        this.orderList = orderList;
    }
   
     public Order addOrder() {
        Order o = new Order();
        orderList.add(o);
        return o;
    }
    
    
    
}
