/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author 65435
 */
public class Supplier extends Person {

    private ProductList productList;

    public Supplier() {
        this.productList = new ProductList();
    }

    public ProductList getProductList() {
        return productList;
    }

    public void setProductList(ProductList productList) {
        this.productList = productList;
    }

    public void initailSupplier(String path, String name) {
        File productFile = new File(path);
        setName(name);
        try {
            Scanner inputsteam = new Scanner(productFile);
            inputsteam.nextLine();
            while (inputsteam.hasNextLine()) {
                Product product = new Product();
                String data = inputsteam.nextLine();
                String[] s = data.split(",");
                getProductList().getProductList().add(product);
                product.setName(s[0]);
                product.setAvail(Integer.parseInt(s[1]));
                product.setBasicPrice(Double.parseDouble(s[2]));
                product.setDescription(s[3]);

            }
            inputsteam.close();
        } catch (Exception e) {
            System.out.println("product");
            e.printStackTrace();
        }

    }
}
