/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class Market {

    private String marketType;
    private CustomerList customerList;
    private double ceilingPar;
    private double floorPar;
    private double targetPar;

    public Market() {
        this.customerList = new CustomerList();
    }

    public String getMarketType() {
        return marketType;
    }

    public void setMarketType(String marketType) {
        this.marketType = marketType;
    }

    public CustomerList getCustomerList() {
        return customerList;
    }

    public void setCustomerList(CustomerList customerList) {
        this.customerList = customerList;
    }

    

    public double getCeilingPar() {
        return ceilingPar;
    }

    public void setCeilingPar(double ceillingPar) {
        this.ceilingPar = ceillingPar;
    }

    public double getFloorPar() {
        return floorPar;
    }

    public void setFloorPar(double floorPar) {
        this.floorPar = floorPar;
    }

    public double getTargetPar() {
        return targetPar;
    }

    public void setTargetPar(double targetPar) {
        this.targetPar = targetPar;
    }
    
}
