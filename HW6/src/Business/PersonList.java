/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author rohan
 */
public class PersonList {

    private ArrayList<Person> personList;

    public PersonList() {
        this.personList = new ArrayList<>();
    }

    public ArrayList<Person> getPersonList() {
        return this.personList;
    }

    public void setPersonList(ArrayList<Person> personList) {
        this.personList = personList;
    }

    public Person searchPerson(String id) {

        for (Person p : this.personList) {
            if (p.getPersonId().equals(id)) {
                return p;
            }
        }
        return null;
    }

    public Person addPerson() {
        Person p = new Person();
        this.personList.add(p);
        return p;
    }

    public void removePerson(Person p) {
        this.personList.remove(p);
    }

}
