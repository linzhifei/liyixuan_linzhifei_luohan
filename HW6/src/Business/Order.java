/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class Order {

    private ArrayList<OrderItem> orderItemList;

    public Order() {
        this.orderItemList = new ArrayList<OrderItem>();
    }

    public ArrayList<OrderItem> getOrderItemList() {
        return orderItemList;
    }

    public void setOrderItemList(ArrayList<OrderItem> orderItemList) {
        this.orderItemList = orderItemList;
    }

    public OrderItem addOrderItem() {
        OrderItem oi = new OrderItem();
        orderItemList.add(oi);
        return oi;
    }

    public void removeOrderItem(OrderItem oi) {
        orderItemList.remove(oi);
    }

    public OrderItem searchOrderItem(String productName) {
        for (OrderItem oi : orderItemList) {
            if (oi.getMarketOffer().getProduct().getName().equals(productName)) {
                return oi;
            }
        }
        return null;
    }

}
