/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author 65435
 */
public class MarketOffer {
    private Product product;
    private double ceiling;
    private double floor;
    private double target;
    private Market market;

    public MarketOffer(){
        product = new Product();
        market = new Market();
    }
    
    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public double getCeiling() {
        return ceiling;
    }

    public void setCeiling(double ceillingPar) {
        this.ceiling = ceiling;
    }

    public double getFloor() {
        return floor;
    }

    public void setFloor(double floorPar) {
        this.floor = floor;
    }

    public double getTarget() {
        return target;
    }

    public void setTarget(double targetPar) {
        this.target = target;
    }

    public Market getMarket() {
        return market;
    }

    public void setMarket(Market market) {
        this.market = market;
    }
    
    
    
    
}
