/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author rohan
 */
public class ConfigurePerson {
    
    public static ArrayList<Person> initial() {
        
        ArrayList<Person> pl =new ArrayList<>();
        Person p;
        
        for (int index = 0; index < 10; index++) {
            p = new Person();
//            String name = "Hulk" + String.valueOf;
            p.setName("Hulk" + index);
            p.setAddress("home");
            p.setPersonId(String.valueOf(500 + index));
            p.setPassword("1");
            p.setPhoneNum(110);
            p.getRoleList().add("admin");
            p.getRoleList().add("Sales Person");
            System.out.println(p.getName());
            pl.add(p);
        }
        
        for (int index = 0; index < 10; index++) {
            p = new Person();
//            String name = "Hulk" + String.valueOf;
            p.setName("thor" + index);
            p.setAddress("home");
            p.setPersonId(String.valueOf(600 + index));
            p.setPassword("1");
            p.setPhoneNum(110);
            p.getRoleList().add("admin");
            p.getRoleList().add("Supplier");
            System.out.println(p.getName());
            pl.add(p);
        }
        
        return pl;
        
    }
    
}
