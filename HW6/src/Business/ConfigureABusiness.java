/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author 65435
 */
public class ConfigureABusiness {

    public static Business initail() {
        Business business = new Business();

        //person
        File personFile = new File("src/Business/Person.csv");
        try {
            Scanner inputsteam = new Scanner(personFile);
            inputsteam.nextLine();
            while (inputsteam.hasNextLine()) {
                Person person = new Person();
                String data = inputsteam.nextLine();
                String[] s = data.split(",");
                business.getPersonList().getPersonList().add(person);
                person.setName(s[0]);
                //System.out.println(person.getName());
                person.setPersonId(s[1]);
                person.setPassword(s[2]);
                person.setAddress(s[3]);
                person.setPhoneNum(Integer.parseInt(s[4]));

                if (s[5].equals("TRUE")) {
                    person.getRoleList().add("admin");
                }
                if (s[6].equals("TRUE")) {
                    person.getRoleList().add("Supplier");
                }
                if (s[7].equals("TRUE")) {
                    person.getRoleList().add("Sales Person");
                }
//                for (int i = 0; i < person.getRoleList().size(); i++) {
//			System.out.println(person.getRoleList().get(i));
//		}
            }
            inputsteam.close();
        } catch (Exception e) {
            System.out.println("person");
            e.printStackTrace();
        }

        //product
        Supplier supplier1 = new Supplier();
        business.getSupplierList().getSupplierList().add(supplier1);
        supplier1.initailSupplier("src/Business/Supplier1.csv", "Many");
        Supplier supplier2 = new Supplier();
        business.getSupplierList().getSupplierList().add(supplier2);
        supplier2.initailSupplier("src/Business/Supplier1.csv", "Ann");
        
        
        //market
        Market educationMarket=new Market();
        business.getMarketList().getMarketList().add(educationMarket);
        educationMarket.setCeilingPar(1.5);
        educationMarket.setFloorPar(1);
        educationMarket.setMarketType("Education");
        educationMarket.setTargetPar(1.25);
        Market financialMarket=new Market();
        business.getMarketList().getMarketList().add(financialMarket);
        financialMarket.setCeilingPar(2);
        financialMarket.setFloorPar(1.5);
        financialMarket.setTargetPar(1.75);

        return business;
    }

}
