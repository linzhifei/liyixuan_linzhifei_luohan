/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class ProductList {
    private ArrayList<Product> productlist;
    
     public ProductList() {
        this.productlist =new ArrayList<Product>();
    }

    public ArrayList<Product> getProductList() {
        return productlist;
    }

    public void setProductList(ArrayList<Product> productcatalog) {
        this.productlist = productlist;
    }
    
    public Product addProduct(Product p){

        productlist.add(p);
        return p;
    }
    
    public void removeProduct(Product p){
        productlist.remove(p);
    }
    
    public Product searchProduct(int id){
        for(Product p:productlist){
            if(p.getModelNum()==id){
                return p;
            }
        }
        return null;
    }
    
    
}
