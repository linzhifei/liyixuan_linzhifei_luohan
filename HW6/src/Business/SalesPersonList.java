/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author rohan
 */
public class SalesPersonList {
    
    private ArrayList<SalesPerson> salesPersonList;
    
    public SalesPersonList(){
        this.salesPersonList = new ArrayList<SalesPerson>();
    }

    public ArrayList<SalesPerson> getSalePersonList() {
        return salesPersonList;
    }

    public void setSalePersonList(ArrayList<SalesPerson> salePersonList) {
        this.salesPersonList = salePersonList;
    }
    
    public SalesPerson addSalesPerson() {
        SalesPerson s = new SalesPerson();
        salesPersonList.add(s);
        return s;
    }

    public void removeSalesPerson(SalesPerson s) {
        salesPersonList.remove(s);
    }

    public SalesPerson searchSalesPerson(String id) {
        for (SalesPerson s : salesPersonList) {
            if (s.getPersonId().equals(id)) {
                return s;
            }
        }
        return null;
    }
    
}
