/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author 65435
 */
public class MarketOfferList {
    
    private ArrayList<MarketOffer> marketOfferList;

    public MarketOfferList() {
        this.marketOfferList=new ArrayList<MarketOffer>();
    }

    
    
    public ArrayList<MarketOffer> getMarketOfferList() {
        return marketOfferList;
    }

    public void setMarketOfferList(ArrayList<MarketOffer> MarketOfferList) {
        this.marketOfferList = MarketOfferList;
    }
    
    public MarketOffer addMarketOffer() {
        MarketOffer m = new MarketOffer();
        marketOfferList.add(m);
        return m;
    }

    public void removeMarketOfferList(MarketOfferList m) {
        marketOfferList.remove(m);
    }

    public MarketOffer searchMarketOffer(String marketName) {
        for (MarketOffer m : marketOfferList) {
            if (m.getMarket().equals(marketName)) {
                return m;
            }
        }
        return null;
    }
    
}
