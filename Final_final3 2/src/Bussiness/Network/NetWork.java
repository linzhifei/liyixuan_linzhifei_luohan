/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Network;

import Bussiness.Enterprise.EnterpriseDirectory;
import Bussiness.Map.Map;
import Bussiness.Map.MapDirectory;
import java.io.Serializable;

/**
 *
 * @author zinanwang
 */
public class NetWork implements Serializable {

    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private Map map;

    public NetWork() {

        enterpriseDirectory = new EnterpriseDirectory();
        
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map getMap() {
        return map;
    }

    public void setMap(Map map) {
        this.map = map;
    }

    @Override
    public String toString() {
        return name;
    }

}
