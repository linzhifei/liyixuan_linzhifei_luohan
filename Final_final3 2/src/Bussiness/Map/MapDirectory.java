/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Map;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author rohan
 */
public class MapDirectory implements Serializable{
    
    private ArrayList<Map> mapDir;

    public MapDirectory() {
        
        mapDir = new ArrayList<>();
        
    }

    public ArrayList<Map> getMapDir() {
        return mapDir;
    }

    public void setMapDir(ArrayList<Map> mapDir) {
        this.mapDir = mapDir;
    }
    
    public Map addMap() {
        Map map = new Map();
        mapDir.add(map);
        return map;
    }
   
}
