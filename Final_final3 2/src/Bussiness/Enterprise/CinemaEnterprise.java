/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Enterprise;

import Bussiness.Role.Role;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public class CinemaEnterprise extends Enterprise implements Serializable{
    
    public CinemaEnterprise(String name){
        super(name, Enterprise.EnterpriseType.Cinema);
    }
    
    @Override
    public ArrayList<Role> getSupportedRole(){
        return null;
    }
    
}
