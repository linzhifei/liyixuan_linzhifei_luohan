/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Enterprise;

import Bussiness.LandMark.LandMark;
import Bussiness.Map.Map;
import Bussiness.Rate.Rate;
import Bussiness.Organization.OrganizationDirectory;
import Bussiness.Organization.Organization;
import Bussiness.Product.ProductDirectory;
import Bussiness.Rate.RateDirectory;
import Bussiness.Rate.RestaurantRate;
import Bussiness.UserAccount.UserAccountDirectory;
import Bussiness.WorkQueue.WorkQueue;
import java.io.Serializable;

/**
 *
 * @author zinanwang
 */
public abstract class Enterprise extends Organization implements Serializable {

    private EnterpriseType enterpriseType;
    private OrganizationDirectory organizationDirectory;
    private Double[] location;
    private RateDirectory rateList;
    private Promo promo;
    private ProductDirectory allProductDirectory;
    private ProductDirectory selectedProductDirectory;
    private WorkQueue workqueue;
    private UserAccountDirectory customerList;
    private Rate averRate;
    private LandMark landMark;
    private String imageRoute;
    private double rate;

    public Enterprise(String name, EnterpriseType type) {
        super(name);
        this.enterpriseType = type;
        organizationDirectory = new OrganizationDirectory();
        location = new Double[2];
        promo = new Promo();
        allProductDirectory = new ProductDirectory();
        selectedProductDirectory = new ProductDirectory();
        rateList = new RateDirectory();
        landMark = new LandMark();
        this.workqueue = new WorkQueue();
        customerList = new UserAccountDirectory();
        this.averRate = new Rate();
    }

    public enum EnterpriseType {

        Restaurant("Restaurant"), Cinema("Cinema"), Hotel("Hotel");

        private String value;

        private EnterpriseType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public UserAccountDirectory getCustomerList() {
        return customerList;
    }

    public void setCustomerList(UserAccountDirectory customerList) {
        this.customerList = customerList;
    }

    public Promo getPromo() {
        return promo;
    }

    public Rate getAverRate() {
        return averRate;
    }

    public void setAverRate(Rate averRate) {
        this.averRate = averRate;
    }

    public WorkQueue getWorkqueue() {
        return workqueue;
    }

    public void setWorkqueue(WorkQueue workqueue) {
        this.workqueue = workqueue;
    }

    public void setPromo(Promo promo) {
        this.promo = promo;
    }

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public Double[] getLocation() {
        return location;
    }

    public void setLocation(Double[] location) {
        this.location = location;
    }

    public RateDirectory getRateList() {
        return rateList;
    }

    public void setRateList(RateDirectory rateList) {
        this.rateList = rateList;
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public ProductDirectory getAllProductDirectory() {
        return allProductDirectory;
    }

    public void setAllProductDirectory(ProductDirectory allProductDirectory) {
        this.allProductDirectory = allProductDirectory;
    }

    public ProductDirectory getSelectedProductDirectory() {
        return selectedProductDirectory;
    }

    public void setSelectedProductDirectory(ProductDirectory selectedProductDirectory) {
        this.selectedProductDirectory = selectedProductDirectory;
    }

    public LandMark getLandMark() {
        return landMark;
    }

    public void setLandMark(LandMark landMark) {
        this.landMark = landMark;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public String getImageRoute() {
        return imageRoute;
    }

    public void setImageRoute(String imageRoute) {
        this.imageRoute = imageRoute;
    }
    
    public Rate createRate() {
        Rate r = null;
        if (this.getEnterpriseType().getValue().equals(Enterprise.EnterpriseType.Restaurant.getValue())) {
            r = new RestaurantRate();
            this.rateList.getRateList().add(r);
        } else {
            r = new Rate();
            this.rateList.getRateList().add(r);
        }
        return r;
    }

}
