/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Enterprise;

import java.io.Serializable;

/**
 *
 * @author 65435
 */
public class Promo implements Serializable{
    private String promoCode;
   private double discount;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }
    
    public String toString(){
        return promoCode;
    }
          
}
