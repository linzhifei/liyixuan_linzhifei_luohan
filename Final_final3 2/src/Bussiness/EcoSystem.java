/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;


import Bussiness.Map.MapDirectory;
import Bussiness.Network.NetWork;
import Bussiness.Organization.Organization;
import Bussiness.Role.Role;
import Bussiness.Role.SystemAdminRole;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public class EcoSystem extends Organization implements Serializable{
    
    private static EcoSystem business;
    private ArrayList<NetWork> networkList;
    private ArrayList<String> imageDir;
    private MapDirectory mapDir;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<NetWork>();
        imageDir = new ArrayList<>();
        mapDir = new MapDirectory();
    }

    public ArrayList<NetWork> getNetworkList() {
        return networkList;
    }

    public MapDirectory getMapDir() {
        return mapDir;
    }

    public void setMapDir(MapDirectory mapDir) {
        this.mapDir = mapDir;
    }

    public ArrayList<String> getImageDir() {
        return imageDir;
    }

    public void setImageDir(ArrayList<String> imageDir) {
        this.imageDir = imageDir;
    }
    
    public NetWork createAndAddNetwork(String name) {
        NetWork network = new NetWork();
        networkList.add(network);
        network.setName(name);
        return network;
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }
    
}
