/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness;

import Bussiness.Employee.Employee;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Enterprise.Promo;
import Bussiness.Map.Map;
import Bussiness.Network.NetWork;
import Bussiness.Organization.Organization;
import Bussiness.Rate.Rate;
import Bussiness.Role.AdminRole;
import Bussiness.Role.FeedbackRole;
import Bussiness.Role.FrontDeskRole;
import Bussiness.Role.KitchenRole;
import Bussiness.Role.SystemAdminRole;
import Bussiness.UserAccount.UserAccount;
import Userinterface.Customer.CusLocation;
import java.io.Serializable;

/**
 *
 * @author zinanwang
 */
public class ConfigASystem implements Serializable{

    public static EcoSystem configure() {

        EcoSystem system = EcoSystem.getInstance();
        Map map1 = system.getMapDir().addMap();
        system.getImageDir().add("Dessert");
        system.getImageDir().add("Hotel");
        system.getImageDir().add("Movie");
        system.getImageDir().add("Coffee");
        //Create a network
        //create an enterprise
        NetWork n = system.createAndAddNetwork("Boston");

        Enterprise res1 = n.getEnterpriseDirectory().createAndAddEnterprise("res1", Enterprise.EnterpriseType.Restaurant);
        Enterprise res2 = n.getEnterpriseDirectory().createAndAddEnterprise("res2", Enterprise.EnterpriseType.Restaurant);
        Enterprise res3 = n.getEnterpriseDirectory().createAndAddEnterprise("res3", Enterprise.EnterpriseType.Restaurant);
        Enterprise cin1 = n.getEnterpriseDirectory().createAndAddEnterprise("cinema1", Enterprise.EnterpriseType.Cinema);
        Enterprise hot1 = n.getEnterpriseDirectory().createAndAddEnterprise("hotel1", Enterprise.EnterpriseType.Hotel);

        //Configure Map1
        map1.setName("map1");
        map1.getLandCordDir().addAndCreateLandMark(45, 110, "LandMark1", true);
        map1.getLandCordDir().addAndCreateLandMark(402, 153, "LandMark2", true);
        map1.getLandCordDir().addAndCreateLandMark(40, 350, "LandMark3", true);
        map1.getLandCordDir().addAndCreateLandMark(460, 250, "LandMark4", true);
        map1.getLandCordDir().addAndCreateLandMark(260, 400, "LandMark5", true);
        map1.getLandCordDir().addAndCreateLandMark(130, 400, "LandMark6", true);
        map1.getLandCordDir().addAndCreateLandMark(45, 275, "LandMark7", true);
        map1.getLandCordDir().addAndCreateLandMark(0, 250, "LandMark8", true);
        map1.getLandCordDir().addAndCreateLandMark(30, 50, "LandMark9", true);
        map1.getLandCordDir().addAndCreateLandMark(390, 20, "LandMark10", true);

        map1.setMapRouteString("image/map1.png");

        //Configure Map2
        n.setMap(system.getMapDir().getMapDir().get(0));

        Promo promo = new Promo();
        promo.setDiscount(0.8);
        promo.setPromoCode("01");

        n.getMap().getLandCordDir().getLandMarkDir().get(0).setAvaliability(false);
        n.getMap().getLandCordDir().getLandMarkDir().get(1).setAvaliability(false);
        n.getMap().getLandCordDir().getLandMarkDir().get(2).setAvaliability(false);
        n.getMap().getLandCordDir().getLandMarkDir().get(3).setAvaliability(false);
        n.getMap().getLandCordDir().getLandMarkDir().get(4).setAvaliability(false);
        
        CusLocation cus0 = new CusLocation();
        cus0.setX(45);
        cus0.setY(110);

        CusLocation cus1 = new CusLocation();
        cus1.setX(402);
        cus1.setY(153);

        CusLocation cus2 = new CusLocation();
        cus2.setX(40);
        cus2.setY(350);

        CusLocation cus3 = new CusLocation();
        cus3.setX(460);
        cus3.setY(250);

        CusLocation cus4 = new CusLocation();
        cus4.setX(260);
        cus4.setY(400);

        res1.setPromo(promo);
        res1.setRate(2.2);
        res1.getLandMark().setCusLocation(cus0);
        res1.setImageRoute("image/Dessert.png");
        cin1.setPromo(promo);
        cin1.setRate(3.8);
        cin1.getLandMark().setCusLocation(cus1);
        cin1.setImageRoute("image/Movie.png");
        hot1.setPromo(promo);
        hot1.setRate(4.2);
        hot1.getLandMark().setCusLocation(cus2);
        hot1.setImageRoute("image/Hotel.png");
        res2.setPromo(promo);
        res2.setRate(3.5);
        res2.getLandMark().setCusLocation(cus3);
        res2.setImageRoute("image/Dessert.png");
        res3.setPromo(promo);
        res3.setRate(4.0);
        res3.getLandMark().setCusLocation(cus4);
        res3.setImageRoute("image/Dessert.png");
        // RestaurantEnterprise res1 = new  RestaurantEnterprise("res1");

        res1.getSelectedProductDirectory().createProduct("pancake", "image/1.jpg", 35);
        res1.getSelectedProductDirectory().createProduct("cereal", "image/2.jpg", 24);
        res1.getSelectedProductDirectory().createProduct("bread", "image/3.jpg", 15);
        res1.getSelectedProductDirectory().createProduct("sandwitch", "image/4.jpg", 20);
        res1.getSelectedProductDirectory().createProduct("croissant", "image/5.jpg", 22);
        res1.getSelectedProductDirectory().createProduct("coffee", "image/6.jpg", 20);

        cin1.getSelectedProductDirectory().createProduct("Dangle", "image/movie1.jpg", 35);
        cin1.getSelectedProductDirectory().createProduct("3 Idiols", "image/movie2.jpg", 35);
        cin1.getSelectedProductDirectory().createProduct("Blade Runner", "image/movie3.jpg", 45);
        cin1.getSelectedProductDirectory().createProduct("Thor", "image/movie4.jpeg", 35);
        cin1.getSelectedProductDirectory().createProduct("LA LA Land", "image/movie8.jpg", 35);
        cin1.getSelectedProductDirectory().createProduct("X-Men", "image/movie6.jpg", 40);

        res2.getSelectedProductDirectory().createProduct("Chocolate Cake", "image/h1.jpeg", 35);
        res2.getSelectedProductDirectory().createProduct("Icecream Cake", "image/h2.jpeg", 35);
        res2.getSelectedProductDirectory().createProduct("Cherry Cake", "image/h3.jpeg", 45);
        res2.getSelectedProductDirectory().createProduct("Matcha Cake", "image/h4.jpeg", 35);
        res2.getSelectedProductDirectory().createProduct("Strawberry Roll", "image/h7.jpeg", 35);
        res2.getSelectedProductDirectory().createProduct("Strawberry Cake", "image/h5.jpeg", 40);

        hot1.getSelectedProductDirectory().createProduct("Room1", "image/hotel1.jpg", 135);
        hot1.getSelectedProductDirectory().createProduct("Room2", "image/hotel2.jpg", 124);
        hot1.getSelectedProductDirectory().createProduct("Room3", "image/hotel3.jpg", 115);
        hot1.getSelectedProductDirectory().createProduct("Room4", "image/hotel4.jpg", 120);
        hot1.getSelectedProductDirectory().createProduct("Room5", "image/hotel5.jpg", 122);
        hot1.getSelectedProductDirectory().createProduct("Room6", "image/hotel6.jpg", 120);
        //initialize some organizations
        //have some employees 
        //create user account

        Employee employee = system.getEmployeeDirectory().createEmployee("RRH");

        UserAccount ua = system.getUserAccountDirectory().createUserAccount("admin", "admin", employee, new SystemAdminRole());

        Employee eee = new Employee();
        eee.setName("firste");
        res1.getUserAccountDirectory().createUserAccount("r1", "1", eee, new AdminRole());
        Organization o1 = res1.getOrganizationDirectory().createOrganization(Organization.Type.Feedback);
        Organization o2 = res1.getOrganizationDirectory().createOrganization(Organization.Type.FrontDesk);
        Organization o3 = res1.getOrganizationDirectory().createOrganization(Organization.Type.Kitchen);
        Employee e1 = o1.getEmployeeDirectory().createEmployee("1");
        Employee ee2 = o2.getEmployeeDirectory().createEmployee("1");
        Employee ee3 = o3.getEmployeeDirectory().createEmployee("1");
        Rate rate1 = new Rate(4, 4, 4, 3, "c1");
        Rate rate2 = new Rate(3, 5, 5, 2, "c2");
        Rate rate3 = new Rate(1, 3, 4, 5, "c3");
        res1.getRateList().getRateList().add(rate3);
        res1.getRateList().getRateList().add(rate1);
        res1.getRateList().getRateList().add(rate2);
        o1.getUserAccountDirectory().createUserAccount("feed", "1", e1, new FeedbackRole());
        o2.getUserAccountDirectory().createUserAccount("front", "1", ee2, new FrontDeskRole());
        o3.getUserAccountDirectory().createUserAccount("k", "1", ee3, new KitchenRole());
        return system;
    }

}
