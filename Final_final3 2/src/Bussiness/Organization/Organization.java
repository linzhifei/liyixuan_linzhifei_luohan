/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Organization;

import Bussiness.Employee.EmployeeCatalog;
import Bussiness.Role.Role;
import Bussiness.UserAccount.UserAccountDirectory;

import Bussiness.WorkQueue.WorkQueue;
import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author zinanwang
 */
public abstract class Organization implements Serializable{
    
    private String name;
    private WorkQueue workQueue;
    private EmployeeCatalog employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    

    private int organizationID;
    private static int counter;
    
    public enum Type{
        Admin("Admin Organization"), Kitchen("Kitchen"),TicketCheck("TicketCheck"),Clean("Clean"),Feedback("Feedback"),FrontDesk("FrontDesk"),Waiter("Waiter");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        this.workQueue = new WorkQueue();
        employeeDirectory = new EmployeeCatalog();
        userAccountDirectory = new UserAccountDirectory();
 
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

  
   

    
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public EmployeeCatalog getEmployeeDirectory() {
        return employeeDirectory;
    }

    public void setEmployeeDirectory(EmployeeCatalog employeeDirectory) {
        this.employeeDirectory = employeeDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public void setOrganizationID(int organizationID) {
        this.organizationID = organizationID;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
}
