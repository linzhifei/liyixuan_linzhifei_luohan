/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Role;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.FeedbackOrganization;
import Bussiness.Organization.Organization;
import Bussiness.UserAccount.UserAccount;
import Userinterface.Feedback.FeedbackWorkAreaJPanel;
import Userinterface.Feedback.ResFeedbackWorkAreaJPanel;
import java.io.Serializable;
import javax.swing.JPanel;

/**
 *
 * @author redbeanlyx
 */
public class ResFeedbackRole extends Role implements Serializable{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
           return new ResFeedbackWorkAreaJPanel(userProcessContainer, enterprise);
    
    }
    
}
