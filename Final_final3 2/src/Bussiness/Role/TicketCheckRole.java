/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Role;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.Organization;
import Bussiness.Organization.TicketCheckOrganization;
import Bussiness.UserAccount.UserAccount;
import Userinterface.TicketCheck.TicketCheckWorkAreaJPanel;
import java.io.Serializable;
import javax.swing.JPanel;

/**
 *
 * @author 65435
 */
public class TicketCheckRole extends Role implements Serializable{
     @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem business) {
        return new TicketCheckWorkAreaJPanel(userProcessContainer,account,(TicketCheckOrganization)organization, enterprise,business);
    }
}
