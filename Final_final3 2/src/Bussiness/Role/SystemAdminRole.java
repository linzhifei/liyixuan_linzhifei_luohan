/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Bussiness.Role;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.Organization;
import Bussiness.UserAccount.UserAccount;
import Userinterface.SystemAdminWorkArea.SystemAdminWorkArea;
import java.io.Serializable;
import javax.swing.JPanel;

/**
 *
 * @author zinanwang
 */
public class SystemAdminRole extends Role implements Serializable{

    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, EcoSystem system) {
       return new SystemAdminWorkArea(userProcessContainer, system);
    }
    
    
}
