/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface;

import Bussiness.EcoSystem;
import Bussiness.Employee.Employee;
import Bussiness.Role.CustomerRole;
import Bussiness.Role.Role;
import Bussiness.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Image;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.Document;

/**
 *
 * @author 65435
 */
public class SignUpJPanel extends javax.swing.JPanel implements DocumentListener {

    private JPanel userProcessContainer;
    private EcoSystem system;
    private UserAccount userAccount;

    /**
     * Creates new form SignUpJPanel
     */
    public SignUpJPanel(JPanel userProcessContainer, EcoSystem system) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.system = system;
        this.userAccount = new UserAccount();
        ImageIcon img = new ImageIcon("image/noname.png");
        img.setImage(img.getImage().getScaledInstance(160, 160, Image.SCALE_DEFAULT));
        lblPhoto.setIcon(img);
        lblPath.setText("");
        userAccount.setPath("");

        ImageIcon img1 = new ImageIcon("image/yes.png");
        img1.setImage(img1.getImage().getScaledInstance(20, 20, Image.SCALE_DEFAULT));
        yes1.setIcon(img1);
        yes2.setIcon(img1);
        yes3.setIcon(img1);
        yes4.setIcon(img1);

        yes1.setVisible(false);
        yes2.setVisible(false);
        yes3.setVisible(false);
        yes4.setVisible(false);

        Document doc = nametext.getDocument();
        Document doc2 = accounttext.getDocument();
        Document doc3 = passwordtext.getDocument();
        Document doc4 = phonetext.getDocument();

        doc.addDocumentListener(this);
        doc2.addDocumentListener(this);
        doc3.addDocumentListener(this);
        doc4.addDocumentListener(this);
    }

    public void choosePic(JLabel jLabel1, JLabel jLabel2) {
        String picturePath = null;
        JFileChooser fd = new JFileChooser();
        fd.showOpenDialog(null);
        File fi = fd.getSelectedFile();
        if (fi != null && fi.getAbsolutePath().endsWith(".jpg")) {
            picturePath = fi.getAbsolutePath();
            ImageIcon img = new ImageIcon(picturePath);
            img.setImage(img.getImage().getScaledInstance(160, 160, Image.SCALE_DEFAULT));
            jLabel1.setIcon(img);
            jLabel2.setText(picturePath);
            userAccount.setPath(picturePath);
            //person.setPhoto(picturePath);

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        accounttext = new javax.swing.JTextField();
        passwordtext = new javax.swing.JTextField();
        phonetext = new javax.swing.JTextField();
        nametext = new javax.swing.JTextField();
        btnSignUp = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        lblPhoto = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnUpload = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        lblPath = new javax.swing.JLabel();
        lblname = new javax.swing.JLabel();
        lbluser = new javax.swing.JLabel();
        lblpass = new javax.swing.JLabel();
        lblphone = new javax.swing.JLabel();
        yes1 = new javax.swing.JLabel();
        yes2 = new javax.swing.JLabel();
        yes3 = new javax.swing.JLabel();
        yes4 = new javax.swing.JLabel();

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel1.setText("Customer Sign Up");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setText("Name:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("User Account:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel4.setText("Password:");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Phone Num:");

        nametext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nametextActionPerformed(evt);
            }
        });

        btnSignUp.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSignUp.setText("Sign Up");
        btnSignUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSignUpActionPerformed(evt);
            }
        });

        btnBack.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        lblPhoto.setText("profile photo");

        jLabel6.setText("profile photo:");

        btnUpload.setText("upload");
        btnUpload.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUploadActionPerformed(evt);
            }
        });

        jLabel7.setText("profile photo path:");

        lblPath.setText("profile photo path");

        lblname.setForeground(new java.awt.Color(51, 51, 51));

        yes1.setText("n");

        yes2.setText("n");

        yes3.setText("n");

        yes4.setText("n");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(accounttext, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nametext)
                                    .addComponent(passwordtext)
                                    .addComponent(phonetext))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(yes1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(yes2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(yes3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(yes4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(32, 32, 32))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblname, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btnSignUp)
                                        .addGap(12, 12, 12)
                                        .addComponent(btnBack))
                                    .addComponent(lbluser, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblpass, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lblphone, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(101, 103, Short.MAX_VALUE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnUpload))
                            .addComponent(jLabel7)
                            .addComponent(lblPath))
                        .addGap(62, 62, 62))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(36, 36, 36)
                .addComponent(jLabel1)
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(btnUpload))
                        .addGap(18, 18, 18)
                        .addComponent(lblPhoto, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblPath))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(nametext, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel2)
                            .addComponent(yes1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblname, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(accounttext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(yes2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbluser, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(passwordtext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(yes3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(11, 11, 11)
                        .addComponent(lblpass, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(phonetext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(yes4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblphone, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(47, 47, 47)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSignUp)
                    .addComponent(btnBack))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);        // TODO add your handling code here:
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnSignUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSignUpActionPerformed
        // TODO add your handling code here:
        String userName = accounttext.getText();

        if (!system.getUserAccountDirectory().checkIfUsernameIsUnique(userName)) {
            JOptionPane.showMessageDialog(null, "The account is existed!");
            return;
        }

        String password = passwordtext.getText();
        if (userName.equals("") || password.equals("")) {
            JOptionPane.showMessageDialog(null, "Account and password cannot be empty!");
            return;
        }

        Pattern pattern = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher = pattern.matcher(nametext.getText());

        Pattern pattern2 = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher2 = pattern2.matcher(accounttext.getText());

        Pattern pattern3 = Pattern.compile("[a-zA-Z]+[0-9]+[a-zA-Z]*|[0-9]+[a-zA-Z]+[0-9]*");
        Matcher matcher3 = pattern3.matcher(passwordtext.getText());

        Pattern pattern4 = Pattern.compile("[0-9]{3}-[0-9]{3}-[0-9]{4}");
        Matcher matcher4 = pattern4.matcher(phonetext.getText());

        if (matcher.matches() == false || matcher2.matches() == false || matcher3.matches() == false
                || matcher4.matches() == false) {
            JOptionPane.showMessageDialog(null, "invalid format!");
            return;
        }

        CustomerRole role = new CustomerRole();
        Employee e = new Employee();
        e.setName(nametext.getName());

        userAccount.setEmployee(e);
        userAccount.setPassword(password);
        userAccount.setRole(role);
        userAccount.setUsername(userName);
        // userAccount.setPath(password);////????????????

        system.getUserAccountDirectory().getUserAccountList().add(userAccount);

        JOptionPane.showMessageDialog(null, "Sign up Successful!");

        accounttext.setText("");
        nametext.setText("");
        passwordtext.setText("");
        phonetext.setText("");
        userProcessContainer.removeAll();
        JPanel blankJP = new JPanel();
        userProcessContainer.add("blank", blankJP);
        CardLayout crdLyt = (CardLayout) userProcessContainer.getLayout();
        crdLyt.next(userProcessContainer);


    }//GEN-LAST:event_btnSignUpActionPerformed

    private void btnUploadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUploadActionPerformed
        // TODO add your handling code here:
        choosePic(lblPhoto, lblPath);
    }//GEN-LAST:event_btnUploadActionPerformed

    private void nametextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nametextActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nametextActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField accounttext;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnSignUp;
    private javax.swing.JButton btnUpload;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel lblPath;
    private javax.swing.JLabel lblPhoto;
    private javax.swing.JLabel lblname;
    private javax.swing.JLabel lblpass;
    private javax.swing.JLabel lblphone;
    private javax.swing.JLabel lbluser;
    private javax.swing.JTextField nametext;
    private javax.swing.JTextField passwordtext;
    private javax.swing.JTextField phonetext;
    private javax.swing.JLabel yes1;
    private javax.swing.JLabel yes2;
    private javax.swing.JLabel yes3;
    private javax.swing.JLabel yes4;
    // End of variables declaration//GEN-END:variables

    @Override
    public void insertUpdate(DocumentEvent e) {

        Pattern pattern = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher = pattern.matcher(nametext.getText());

        Pattern pattern2 = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher2 = pattern2.matcher(accounttext.getText());

        Pattern pattern3 = Pattern.compile("[a-zA-Z]+[0-9]+[a-zA-Z]*|[0-9]+[a-zA-Z]+[0-9]*");
        Matcher matcher3 = pattern3.matcher(passwordtext.getText());

        Pattern pattern4 = Pattern.compile("[0-9]{3}-[0-9]{3}-[0-9]{4}");
        Matcher matcher4 = pattern4.matcher(phonetext.getText());

        if (matcher.matches() == false) {
            lblname.setText("at least 3 characters!");
            lblname.setForeground(Color.red);
            yes1.setVisible(false);
        } else {
            lblname.setText("valid");
            lblname.setForeground(Color.green);
            yes1.setVisible(true);
        }

        if (matcher2.matches() == false) {
            lbluser.setText("at least 3 characters!");
            lbluser.setForeground(Color.red);
            yes2.setVisible(false);
        } else {
            lbluser.setText("valid");
            lbluser.setForeground(Color.green);
            yes2.setVisible(true);
        }

        if (matcher3.matches() == false) {
            lblpass.setText("not a strong password!");
            lblpass.setForeground(Color.red);
            yes3.setVisible(false);
        } else {
            lblpass.setText("valid");
            lblpass.setForeground(Color.green);
            yes3.setVisible(true);
        }

        if (matcher4.matches() == false) {
            lblphone.setText("correct format: 617-888-8888");
            lblphone.setForeground(Color.red);
            yes4.setVisible(false);
        } else {
            lblphone.setText("valid");
            lblphone.setForeground(Color.green);
            yes4.setVisible(true);
        }

    }

    @Override
    public void removeUpdate(DocumentEvent e) {

        Pattern pattern = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher = pattern.matcher(nametext.getText());

        Pattern pattern2 = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher2 = pattern2.matcher(accounttext.getText());

        Pattern pattern3 = Pattern.compile("[a-zA-Z]+[0-9]+[a-zA-Z]*|[0-9]+[a-zA-Z]+[0-9]*");
        Matcher matcher3 = pattern3.matcher(passwordtext.getText());

        Pattern pattern4 = Pattern.compile("[0-9]{3}-[0-9]{3}-[0-9]{4}");
        Matcher matcher4 = pattern4.matcher(phonetext.getText());

        if (matcher.matches() == false) {
            lblname.setText("at least 3 characters!");
            lblname.setForeground(Color.red);
            yes1.setVisible(false);
        } else {
            lblname.setText("valid");
            lblname.setForeground(Color.green);
            yes1.setVisible(true);
        }

        if (matcher2.matches() == false) {
            lbluser.setText("at least 3 characters!");
            lbluser.setForeground(Color.red);
            yes2.setVisible(false);
        } else {
            lbluser.setText("valid");
            lbluser.setForeground(Color.green);
            yes2.setVisible(true);
        }

        if (matcher3.matches() == false) {
            lblpass.setText("not a strong password!");
            lblpass.setForeground(Color.red);
            yes3.setVisible(false);
        } else {
            lblpass.setText("valid");
            lblpass.setForeground(Color.green);
            yes3.setVisible(true);
        }

        if (matcher4.matches() == false) {
            lblphone.setText("correct format: 617-888-8888");
            lblphone.setForeground(Color.red);
            yes4.setVisible(false);
        } else {
            lblphone.setText("valid");
            lblphone.setForeground(Color.green);
            yes4.setVisible(true);
        }

    }

    @Override
    public void changedUpdate(DocumentEvent e) {

        Pattern pattern = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher = pattern.matcher(nametext.getText());

        Pattern pattern2 = Pattern.compile("[a-zA-Z]{3}[a-zA-Z]*");
        Matcher matcher2 = pattern2.matcher(accounttext.getText());

        Pattern pattern3 = Pattern.compile("[a-zA-Z]+[0-9]+[a-zA-Z]*|[0-9]+[a-zA-Z]+[0-9]*");
        Matcher matcher3 = pattern3.matcher(passwordtext.getText());

        Pattern pattern4 = Pattern.compile("[0-9]{3}-[0-9]{3}-[0-9]{4}");
        Matcher matcher4 = pattern4.matcher(phonetext.getText());

        if (matcher.matches() == false) {
            lblname.setText("at least 3 characters!");
            lblname.setForeground(Color.red);
            yes1.setVisible(false);
        } else {
            lblname.setText("valid");
            lblname.setForeground(Color.green);
            yes1.setVisible(true);
        }

        if (matcher2.matches() == false) {
            lbluser.setText("at least 3 characters!");
            lbluser.setForeground(Color.red);
            yes2.setVisible(false);
        } else {
            lbluser.setText("valid");
            lbluser.setForeground(Color.green);
            yes2.setVisible(true);
        }

        if (matcher3.matches() == false) {
            lblpass.setText("not a strong password!");
            lblpass.setForeground(Color.red);
            yes3.setVisible(false);
        } else {
            lblpass.setText("valid");
            lblpass.setForeground(Color.green);
            yes3.setVisible(true);
        }

        if (matcher4.matches() == false) {
            lblphone.setText("correct format: 617-888-8888");
            lblphone.setForeground(Color.red);
            yes4.setVisible(false);
        } else {
            lblphone.setText("valid");
            lblphone.setForeground(Color.green);
            yes4.setVisible(true);
        }

    }
}
