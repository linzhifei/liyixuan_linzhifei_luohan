/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.AdminWorkArea;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Network.NetWork;
import Bussiness.Rate.Rate;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jfree.chart.ChartPanel;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author 65435
 */
public class ManageFeedbackJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ManageFeedbackJPanel
     */
    private ChartPanel frame1;
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private EcoSystem system;

    public ManageFeedbackJPanel(JPanel userProcessContainer, Enterprise enterprise,EcoSystem e) {

        initComponents();

        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.system=e;
        try{
        CategoryDataset dataset = getDataSet();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No feedback!");
            return;
        }
        int toprice=0;
        int toservice=0;
        int toenvironment=0;
        int averP=0;
        int averS=0;
        int averE=0;
        int num=0;
        for(NetWork n: system.getNetworkList()){
            for(Enterprise en: n.getEnterpriseDirectory().getEnterpriseList()){
                if(!en.getAverRate().equals(null)){
                    num++;
                    toprice+=en.getAverRate().getPriceRate();
                    toservice+=en.getAverRate().getServiceRate();
                    toenvironment+=en.getAverRate().getEnvironmentRate();
                }
            }
        }
        averP=toservice/num;
        averS=toservice/num;
        averE=toenvironment/num;
        
        if(this.enterprise.getAverRate().getPriceRate()>=averP){
            txtPrice1.setText("Above Average");
        }else{
            txtPrice1.setText("Below Average");
        }
         if(this.enterprise.getAverRate().getServiceRate()>=averS){
            txtService1.setText("Above Average");
        }else{
            txtService1.setText("Below Average");
        }
          if(this.enterprise.getAverRate().getEnvironmentRate()>=averE){
            txtEn1.setText("Above Average");
        }else{
            txtEn1.setText("Below Average");
        }
    }

    private CategoryDataset getDataSet() {

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        int price1 = 0;
        int price2 = 0;
        int price3 = 0;
        int price4 = 0;
        int price5 = 0;

        int en1 = 0;
        int en2 = 0;
        int en3 = 0;
        int en4 = 0;
        int en5 = 0;

        int service1 = 0;
        int service2 = 0;
        int service3 = 0;
        int service4 = 0;
        int service5 = 0;

        int averagePrice = 0;
        int averageEnviroment = 0;
        int averageService = 0;

        for (Rate r : enterprise.getRateList().getRateList()) {

            if (r.getPriceRate() == 1) {
                price1++;
            } else if (r.getPriceRate() == 2) {
                price2++;
            } else if (r.getPriceRate() == 3) {
                price3++;
            } else if (r.getPriceRate() == 4) {
                price4++;
            } else {
                price5++;
            }

            if (r.getEnvironmentRate() == 1) {
                en1++;
            } else if (r.getEnvironmentRate() == 2) {
                en2++;
            } else if (r.getEnvironmentRate() == 3) {
                en3++;
            } else if (r.getEnvironmentRate() == 4) {
                en4++;
            } else {
                en5++;
            }

            if (r.getServiceRate() == 1) {
                service1++;
            } else if (r.getServiceRate() == 2) {
                service2++;
            } else if (r.getServiceRate() == 3) {
                service3++;
            } else if (r.getServiceRate() == 4) {
                service4++;
            } else {
                service5++;
            }

        }

        averagePrice = (price1 * 1 + price2 * 2 + price3 * 3 + price4 * 4 + price5 * 5) / (price1 + price2 + price3 + price4 + price5);
        averageEnviroment = (en1 * 1 + en2 * 2 + en3 * 3 + en4 * 4 + en5 * 5) / (en1 + en2 + en3 + en4 + en5);
        averageService = (service1 * 1 + service2 * 2 + service3 * 3 + service4 * 4 + service5 * 5) / (service1 + service2 + service3 + service4 + service5);

        txtPrice.setText(String.valueOf(averagePrice));
        txtEn.setText(String.valueOf(averageEnviroment));
        txtService.setText(String.valueOf(averageService));
        enterprise.getAverRate().setPriceRate(averagePrice);
        enterprise.getAverRate().setEnvironmentRate(averageEnviroment);
        enterprise.getAverRate().setServiceRate(averageService);

        dataset.addValue(price1, "price", "1");
        dataset.addValue(en1, "enviroment", "1");
        dataset.addValue(service1, "service", "1");
        dataset.addValue(price2, "price", "2");
        dataset.addValue(en2, "enviroment", "2");
        dataset.addValue(service2, "service", "2");
        dataset.addValue(price3, "price", "3");
        dataset.addValue(en3, "enviroment", "3");
        dataset.addValue(service3, "service", "3");
        dataset.addValue(price4, "price", "4");
        dataset.addValue(en4, "enviroment", "4");
        dataset.addValue(service4, "service", "4");
        dataset.addValue(price5, "price", "5");
        dataset.addValue(en5, "enviroment", "5");
        dataset.addValue(service5, "service", "5");

        return dataset;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        txtEn = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txtService = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtPrice1 = new javax.swing.JTextField();
        txtEn1 = new javax.swing.JTextField();
        txtService1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        jLabel4.setFont(new java.awt.Font("Lucida Grande", 1, 18)); // NOI18N
        jLabel4.setText("Manage Feedback");

        jLabel3.setText("Average Price Rating:");

        txtPrice.setEnabled(false);
        txtPrice.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPriceActionPerformed(evt);
            }
        });

        txtEn.setEnabled(false);

        jLabel1.setText("Average Enviroment Rating:");

        txtService.setEnabled(false);

        jLabel2.setText("Average Service Rating:");

        txtPrice1.setEnabled(false);
        txtPrice1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrice1ActionPerformed(evt);
            }
        });

        txtEn1.setEnabled(false);

        txtService1.setEnabled(false);

        jButton1.setText("Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Adjust Menu");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(208, 208, 208)
                        .addComponent(jLabel4))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(71, 71, 71)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jButton1))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(78, 78, 78)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtService, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtEn, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(46, 46, 46)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtService1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtEn1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txtPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(161, 161, 161)
                                .addComponent(jButton2)))))
                .addContainerGap(193, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addComponent(jLabel4)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(txtPrice, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel1)
                            .addComponent(txtEn, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txtService, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtPrice1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(txtEn1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(23, 23, 23)
                        .addComponent(txtService1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 175, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addGap(94, 94, 94))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtPriceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPriceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPriceActionPerformed

    private void txtPrice1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrice1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrice1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        ManageMenu2JPanel muajp = new ManageMenu2JPanel(userProcessContainer, enterprise);
        userProcessContainer.add("ManageUserAccountJPanel", muajp);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JTextField txtEn;
    private javax.swing.JTextField txtEn1;
    private javax.swing.JTextField txtPrice;
    private javax.swing.JTextField txtPrice1;
    private javax.swing.JTextField txtService;
    private javax.swing.JTextField txtService1;
    // End of variables declaration//GEN-END:variables
}
