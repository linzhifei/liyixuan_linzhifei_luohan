/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.Customer;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.FrontDeskOrganization;
import Bussiness.Organization.KitchenOrganization;
import Bussiness.Organization.Organization;
import Bussiness.Organization.WaiterOrganization;
import Bussiness.Product.Product;
import Bussiness.UserAccount.UserAccount;
import Bussiness.WorkQueue.OrderRequest;
import Bussiness.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rohan
 */
public class InResJPanel extends javax.swing.JPanel {

    /**
     * Creates new form InResJPanel
     */
    private EcoSystem system;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private JPanel userProcessContainer;
    private OrderRequest orderRequest;

    public class RequestThread implements Runnable {

        @Override
        public void run() {
            ServerSocket ss = null;
            try {
                //让服务器端程序开始监听来自4242端口的客户端请求  
                if (ss == null) {
                    ss = new ServerSocket(4243);
                    System.out.println("server start running...");
                }
                // System.out.println(orderRequest.getProductName());
                //服务器无穷的循环等待客户端的请求  
                while (true) {
                    Socket s = ss.accept();
                    ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
                    out.writeObject(orderRequest);
                    System.out.println("done!");
//System.out.println(orderRequest.getProductName());
                    out.flush();

//                    InputStreamReader inputStreamReader = new InputStreamReader(s.getInputStream());
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//
//                    String request = bufferedReader.readLine();
//
//                    System.out.println("接收到了客户端的请求:" + request);
//                    PrintWriter printWriter = new PrintWriter(s.getOutputStream());
//
//                    String advice = "You connect to server successful";
//                    printWriter.println(advice);
//                    printWriter.close();
                    out.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block  
                e.printStackTrace();
            }
        }

    }

    public InResJPanel(JPanel userProcessContainer, Enterprise enterprise, UserAccount userAccount) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.orderRequest = new OrderRequest();
        boolean flag = false;
        for (WorkRequest w : userAccount.getWorkQueue().getWorkRequestList()) {
            if (w.getEnterprise() == enterprise) {
                flag = true;
                break;
            }
        }
        if (flag == true) {
            int dialogButton = JOptionPane.YES_NO_OPTION;
            int dialogResult = JOptionPane.showConfirmDialog(null, "wirte feedback for this restaurant?", "remind", dialogButton);
            if (dialogResult == JOptionPane.YES_OPTION) {
                //   CustomerResFeedbackJPanel panel = new CustomerResFeedbackJPanel();
                //  userProcessContainer.add("CustomerResFeedbackJPanel", panel);
                CardLayout layout = (CardLayout) userProcessContainer.getLayout();
                layout.next(userProcessContainer);

            }
        }
        populateMenu();
        populateOrderTable();

    }

    public void populateMenu() {

        ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        String picturePath1 = spa.get(0).getImagePath();
        String picturePath2 = spa.get(1).getImagePath();
        String picturePath3 = spa.get(2).getImagePath();
        String picturePath4 = spa.get(3).getImagePath();
        String picturePath5 = spa.get(4).getImagePath();
        String picturePath6 = spa.get(5).getImagePath();

        ImageIcon img1 = new ImageIcon(picturePath1);
        ImageIcon img2 = new ImageIcon(picturePath2);
        ImageIcon img3 = new ImageIcon(picturePath3);
        ImageIcon img4 = new ImageIcon(picturePath4);
        ImageIcon img5 = new ImageIcon(picturePath5);
        ImageIcon img6 = new ImageIcon(picturePath6);
        System.out.println(img1);
        img1.setImage(img1.getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT));
        img2.setImage(img2.getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT));
        img3.setImage(img3.getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT));
        img4.setImage(img4.getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT));
        img5.setImage(img5.getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT));
        img6.setImage(img6.getImage().getScaledInstance(100, 150, Image.SCALE_DEFAULT));

        picture1.setIcon(img1);
        picture2.setIcon(img2);
        picture3.setIcon(img3);
        picture4.setIcon(img4);
        picture5.setIcon(img5);
        picture6.setIcon(img6);

        product1.setText(spa.get(0).getName());
        product2.setText(spa.get(1).getName());
        product3.setText(spa.get(2).getName());
        product4.setText(spa.get(3).getName());
        product5.setText(spa.get(4).getName());
        product6.setText(spa.get(5).getName());

        for (WorkRequest w : userAccount.getWorkQueue().getWorkRequestList()) {
//            if (w.getStatus().equals("")) {
                Product p = w.getProductName();
                for (int i = 0; i < spa.size(); i++) {
                    if (spa.get(i).getName().equals(p.getName())) {
                        if (i == 0) {
                            picture1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 153), 3));
                        } else if (i == 1) {
                            picture2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 153), 3));
                        } else if (i == 2) {
                            picture3.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 153), 3));
                        } else if (i == 3) {
                            picture4.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 153), 3));
                        } else if (i == 4) {
                            picture5.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 153), 3));
                        } else if (i == 5) {
                            picture6.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(255, 102, 153), 3));
                        }

                    }
//                }
            }

        }

    }

    public void populateOrderTable() {
        ////////////??????????????????????????enterprise
        DefaultTableModel model = (DefaultTableModel) orderedItemTable.getModel();
        model.setRowCount(0);

        for (WorkRequest w : userAccount.getWorkQueue().getWorkRequestList()) {
            if (w.getEnterprise() == enterprise && w.getStatus().equals("customer order")) {
                Object[] row = new Object[4];
                row[1] = w.getProductName();
                row[2] = w.getPrice();
                row[3] = w.getStatus();
                row[0] = w;
                model.addRow(row);
            }

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        orderedItemTable = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        submitBtn = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        picture2 = new javax.swing.JLabel();
        picture4 = new javax.swing.JLabel();
        picture5 = new javax.swing.JLabel();
        picture6 = new javax.swing.JLabel();
        picture3 = new javax.swing.JLabel();
        picture1 = new javax.swing.JLabel();
        product1 = new javax.swing.JRadioButton();
        product4 = new javax.swing.JRadioButton();
        product3 = new javax.swing.JRadioButton();
        product2 = new javax.swing.JRadioButton();
        product5 = new javax.swing.JRadioButton();
        product6 = new javax.swing.JRadioButton();
        txtPromo = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jRadioButton1 = new javax.swing.JRadioButton();

        orderedItemTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "id", "Item Name", "Price", "Status"
            }
        ));
        jScrollPane1.setViewportView(orderedItemTable);

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 3, 14)); // NOI18N
        jLabel1.setText("Ordered List");

        submitBtn.setText("Submit");
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitBtnActionPerformed(evt);
            }
        });

        jButton1.setText("<<Back");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Lucida Grande", 3, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Menu");

        product1.setText("product1");
        product1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product1ActionPerformed(evt);
            }
        });

        product4.setText("product4");
        product4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product4ActionPerformed(evt);
            }
        });

        product3.setText("product3");
        product3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product3ActionPerformed(evt);
            }
        });

        product2.setText("product2");
        product2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product2ActionPerformed(evt);
            }
        });

        product5.setText("product5");
        product5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product5ActionPerformed(evt);
            }
        });

        product6.setText("product6");
        product6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                product6ActionPerformed(evt);
            }
        });

        jLabel3.setText("promotion code:");

        jRadioButton1.setText("Urgent");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(124, 124, 124)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(155, 155, 155))
            .addGroup(layout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(picture5, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(picture3, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(picture1, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(product1)
                    .addComponent(product3)
                    .addComponent(product5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 114, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(picture4, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(picture6, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(picture2, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
                    .addComponent(product4)
                    .addComponent(product6)
                    .addComponent(product2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 295, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(4, 4, 4)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(txtPromo, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(submitBtn))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel3)
                                    .addGap(0, 0, Short.MAX_VALUE)))))
                    .addComponent(jRadioButton1))
                .addGap(36, 36, 36))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(picture1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture2, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(product1)
                            .addComponent(product2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(picture3, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(product3)
                            .addComponent(product4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(picture5, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(picture6, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(24, 24, 24)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(product5)
                            .addComponent(product6))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGap(44, 44, 44)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 346, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtPromo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(submitBtn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jRadioButton1)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void product1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product1ActionPerformed

    private void product4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product4ActionPerformed

    private void product3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product3ActionPerformed

    private void product2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product2ActionPerformed

    private void product5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product5ActionPerformed

    private void product6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_product6ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_product6ActionPerformed

    private void submitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitBtnActionPerformed
         // TODO add your handling code here:
        ArrayList<Product> spa = enterprise.getSelectedProductDirectory().getProductDirectory();
        int[] checkIfSelected = new int[6];
        if (product1.isSelected()) {
            checkIfSelected[0] = 1;
        }
        if (product2.isSelected()) {
            checkIfSelected[1] = 1;
        }
        if (product3.isSelected()) {
            checkIfSelected[2] = 1;
        }
        if (product4.isSelected()) {
            checkIfSelected[3] = 1;
        }
        if (product5.isSelected()) {
            checkIfSelected[4] = 1;
        }
        if (product6.isSelected()) {
            checkIfSelected[5] = 1;
        }

        for (int i = 0; i < 6; i++) {
            if (checkIfSelected[i] != 0) {

                OrderRequest w = new OrderRequest();

                w.setCustomer(userAccount.getUsername());/////////////
                w.setPrice(spa.get(i).getProductPrice());
                w.setProductName(spa.get(i));
                w.setStatus("customer order");///////////???????????
                w.setSender(userAccount);
                w.setEnterprise(enterprise);

                //  System.out.println(w.getProductName());
                if (!txtPromo.getText().equals("")) {
                    if (txtPromo.getText().equals(enterprise.getPromo().getPromoCode())) {
                        w.setPromoCode(txtPromo.getText());
                        w.setPrice((int) (spa.get(i).getProductPrice() * enterprise.getPromo().getDiscount()));
                    } else {
                        JOptionPane.showMessageDialog(null, "invalid promotion code!");
                        return;
                    }
                }

                Date startTime = new Date();
                w.setStartTime(startTime);///////////////?????????

                if(jRadioButton1.isSelected()){
                    w.setUrgent(true);
                }
              
                userAccount.getWorkQueue().getWorkRequestList().add(w);
                enterprise.getWorkqueue().getWorkRequestList().add(w);

                orderRequest = w;

                /* --------------------THREAD------------------------*/
                RequestThread tt = new RequestThread();
                Thread t = new Thread(tt);
                t.start();
                /*---------------------------------------------------*/

                Organization org = null;
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (organization instanceof FrontDeskOrganization) {
                        org = organization;
                        break;
                    }
                }
                if (org != null) {

                    org.getWorkQueue().getWorkRequestList().add(w);
                    //organization.getWorkQueue().getWorkRequestList().add(w);
                }

            }
            populateOrderTable();
        }


    }//GEN-LAST:event_submitBtnActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_jButton1ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable orderedItemTable;
    private javax.swing.JLabel picture1;
    private javax.swing.JLabel picture2;
    private javax.swing.JLabel picture3;
    private javax.swing.JLabel picture4;
    private javax.swing.JLabel picture5;
    private javax.swing.JLabel picture6;
    private javax.swing.JRadioButton product1;
    private javax.swing.JRadioButton product2;
    private javax.swing.JRadioButton product3;
    private javax.swing.JRadioButton product4;
    private javax.swing.JRadioButton product5;
    private javax.swing.JRadioButton product6;
    private javax.swing.JButton submitBtn;
    private javax.swing.JTextField txtPromo;
    // End of variables declaration//GEN-END:variables

}
