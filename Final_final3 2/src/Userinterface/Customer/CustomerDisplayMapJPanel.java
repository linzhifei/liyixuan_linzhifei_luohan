/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.Customer;

import Bussiness.Enterprise.Enterprise;
import Bussiness.LandMark.LandMark;
import Bussiness.Network.NetWork;
import Bussiness.UserAccount.UserAccount;
import java.awt.CardLayout;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author rohan
 */
public class CustomerDisplayMapJPanel extends JPanel implements ActionListener {

    private Image image = null;
    private Enterprise enterprise = null;
    private JPanel userProcessContainer = null;
    private JButton upBtn = null;
    private JButton downBtn = null;
    private JButton leftBtn = null;
    private JButton rightBtn = null;
    private JButton searchBtn = null;
    private JButton enterBtn = null;
    private JButton backBtn = null;
    private UserAccount account = null;
    private CusLocation cusLocation = null;
    private CusLocation targetLocation = null;
    private CusLocation resetLocation = null;
    private NetWork n = null;
    private Image userImg = null;
    private Image tarImg = null;
    private SearchJFrame searchJFrame = null;

    public CustomerDisplayMapJPanel(Image image, JPanel userProcessContainer, UserAccount account, NetWork n) {

        this.setLayout(null);
        this.requestFocusInWindow();
        cusLocation = new CusLocation();
        targetLocation = new CusLocation();
        resetLocation = new CusLocation();
        userImg = new ImageIcon("image/user.png").getImage();
        tarImg = new ImageIcon("image/tar.png").getImage();

        cusLocation.setX(255);
        cusLocation.setY(270);
        targetLocation.setX(0);
        targetLocation.setY(0);
        resetLocation.setX(0);
        resetLocation.setY(0);

        upBtn = new JButton("UP");
        upBtn.addActionListener(this);
        leftBtn = new JButton("LEFT");
        leftBtn.addActionListener(this);
        rightBtn = new JButton("RIGHT");
        rightBtn.addActionListener(this);
        downBtn = new JButton("DOWN");
        downBtn.addActionListener(this);
        searchBtn = new JButton("Search");
        searchBtn.addActionListener(this);
        enterBtn = new JButton("Enter");
        enterBtn.addActionListener(this);
        backBtn = new JButton("<<Back");
        backBtn.addActionListener(this);

        JLabel indication1 = new JLabel("Control Panel");
        JLabel indication2 = new JLabel("Search Store");
        JLabel indication3 = new JLabel("Enter Store");

        this.setSize(700, 600);
        this.add(upBtn);
        this.add(leftBtn);
        this.add(rightBtn);
        this.add(downBtn);
        this.add(searchBtn);
        this.add(enterBtn);
        this.add(backBtn);
        this.add(indication1);
        this.add(indication2);
        this.add(indication3);

        upBtn.setBounds(76, 492, 65, 25);
        leftBtn.setBounds(10, 517, 65, 25);
        rightBtn.setBounds(146, 517, 65, 25);
        downBtn.setBounds(78, 517, 65, 25);
        searchBtn.setBounds(270, 492, 70, 25);
        enterBtn.setBounds(452, 492, 70, 25);
        backBtn.setBounds(620, 492, 80, 25);
        indication1.setBounds(73, 470, 80, 25);
        indication2.setBounds(268, 470, 80, 25);
        indication3.setBounds(450, 470, 80, 25);

        ImageIcon icon = new ImageIcon("image/user.png");
        icon.setImage(icon.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));

        this.setVisible(true);
        this.image = image;
        this.account = account;
        this.n = n;
        this.userProcessContainer = userProcessContainer;
        DisplayStore();
    }

    public void DisplayStore() {
        int i = 0;
        for (Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()) {
            ImageIcon icon = new ImageIcon(e.getImageRoute());
            System.out.println(e.getImageRoute());
            icon.setImage(icon.getImage().getScaledInstance(40, 40, Image.SCALE_DEFAULT));
            JLabel land = new JLabel("landMark" + i);
            this.add(land);
            land.setBounds(e.getLandMark().getCusLocation().getX(), e.getLandMark().getCusLocation().getY(), 45, 45);
            land.setIcon(icon);
        }
    }

    public CusLocation getTargetLocation() {
        return targetLocation;
    }

    public void setTargetLocation(CusLocation targetLocation) {
        this.targetLocation = targetLocation;
    }

    public void GoingUp() {
        cusLocation.setY(cusLocation.getY() - 5);
        repaint();
    }

    public void GoingDown() {
        cusLocation.setY(cusLocation.getY() + 5);
        repaint();
    }

    public void GoingLeft() {
        cusLocation.setX(cusLocation.getX() - 5);
        repaint();
    }

    public void GoingRight() {
        cusLocation.setX(cusLocation.getX() + 5);
        repaint();
    }

    public void setBtn() {
        searchBtn.setEnabled(true);
        enterBtn.setEnabled(true);
    }

    public CusLocation getCusLoc() {
        return this.cusLocation;
    }

    public CusLocation getResetLocation() {
        return resetLocation;
    }

    public void setResetLocation(CusLocation resetLocation) {
        this.resetLocation = resetLocation;
    }

    public int GetDistance(CusLocation cus, LandMark tar) {
        int dis;
        dis = (int) Math.pow(Math.pow(cus.getX() - tar.getCusLocation().getX(), 2) + Math.pow(cus.getY() - tar.getCusLocation().getY(), 2), 0.5);
        return dis;
    }

    public void paintComponent(Graphics g) {
        g.drawImage(image, 0, 0, 750, 450, this);
        g.drawImage(userImg, cusLocation.getX(), cusLocation.getY(), 30, 40, this);
        g.drawImage(tarImg, targetLocation.getX() - 10, targetLocation.getY() - 10, resetLocation.getX(), resetLocation.getY(), this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == upBtn) {
            GoingUp();
        }

        if (e.getSource() == downBtn) {
            GoingDown();
        }

        if (e.getSource() == leftBtn) {
            GoingLeft();
        }

        if (e.getSource() == rightBtn) {
            GoingRight();
        }

        if (e.getSource() == searchBtn) {

            searchJFrame = new SearchJFrame(account, n, this);
            searchJFrame.setVisible(true);
            searchBtn.setEnabled(false);
            enterBtn.setEnabled(false);
            targetLocation.setX(0);
            targetLocation.setY(0);
            resetLocation.setX(0);
            resetLocation.setY(0);
            repaint();

        }

        if (e.getSource() == enterBtn) {
            int flag = 0;
            for (Enterprise en : n.getEnterpriseDirectory().getEnterpriseList()) {
                if (GetDistance(cusLocation, en.getLandMark()) <= 10) {
                    System.out.println(en.getName()+en.getSelectedProductDirectory().getProductDirectory().size());
                   if (!en.getSelectedProductDirectory().getProductDirectory().isEmpty()) {
                        System.out.println("distance in " + GetDistance(cusLocation, en.getLandMark()));
                        InResJPanel irjp = new InResJPanel(userProcessContainer, en, account);
                        userProcessContainer.add("InResJPanel", irjp);
                        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
                        layout.next(userProcessContainer);
                        flag = 0;
                        return;
                    } else {
                       JOptionPane.showMessageDialog(null, "The store is close.");
                    return;
                    }
                } else {
                     
                     flag = 1;
                }
              
            }
              if (flag == 1) {
                    JOptionPane.showMessageDialog(null, "There are no store in your area.");
                }
        }

        if (e.getSource() == backBtn) {
            userProcessContainer.remove(this);
            CardLayout layout = (CardLayout) userProcessContainer.getLayout();
            layout.previous(userProcessContainer);
        }
    }

}
