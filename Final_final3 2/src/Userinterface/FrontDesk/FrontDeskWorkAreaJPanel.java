/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Userinterface.FrontDesk;

import Bussiness.EcoSystem;
import Bussiness.Enterprise.Enterprise;
import Bussiness.Organization.CleanOrganization;
import Bussiness.Organization.FeedbackOrganization;
import Bussiness.Organization.FrontDeskOrganization;
import Bussiness.Organization.KitchenOrganization;
import Bussiness.Organization.Organization;
import Bussiness.Organization.TicketCheckOrganization;
import Bussiness.UserAccount.UserAccount;
import Bussiness.WorkQueue.OrderRequest;
import Bussiness.WorkQueue.WorkRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author 65435
 */
public class FrontDeskWorkAreaJPanel extends javax.swing.JPanel {

    /**
     * Creates new form FrontDeskWorkAreaJPanel
     */
    private JPanel userProcessContainer;
    private FrontDeskOrganization organization;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private EcoSystem system;
    private OrderRequest orderRequest;

    public class RequestThread implements Runnable {

        @Override
        public void run() {
            ServerSocket ss = null;
            try {
                //让服务器端程序开始监听来自4242端口的客户端请求  
                if (ss == null) {
                    ss = new ServerSocket(5243);
                    System.out.println("front desk server start running...");
                }
                // System.out.println(orderRequest.getProductName());
                //服务器无穷的循环等待客户端的请求  
                while (true) {
                    Socket s = ss.accept();
                    ObjectOutputStream out = new ObjectOutputStream(s.getOutputStream());
                    out.writeObject(orderRequest);
                    System.out.println("done!");
//System.out.println(orderRequest.getProductName());
                    out.flush();

//                    InputStreamReader inputStreamReader = new InputStreamReader(s.getInputStream());
//                    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//
//                    String request = bufferedReader.readLine();
//
//                    System.out.println("接收到了客户端的请求:" + request);
//                    PrintWriter printWriter = new PrintWriter(s.getOutputStream());
//
//                    String advice = "You connect to server successful";
//                    printWriter.println(advice);
//                    printWriter.close();
                    out.close();
                }
            } catch (IOException e) {
                // TODO Auto-generated catch block  
                e.printStackTrace();
            }
        }

    }

    public FrontDeskWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, FrontDeskOrganization organization, Enterprise enterprise, EcoSystem business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.userAccount = account;
        this.orderRequest = new OrderRequest();
        nametext.setText(account.getEmployee().getName());
        accounttext.setText(account.getUsername());
        populate();
    }

    public void populate() {

        DefaultTableModel dtm = (DefaultTableModel) jTable1.getModel();

        dtm.setRowCount(0);

        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (request.isUrgent()) {
                Object[] row = new Object[7];
                row[0] = request;
                row[1] = request.getCustomer();
                row[2] = request.getPrice();
                row[3] = request.getSender();
                row[4] = request.getStatus();
                row[5] = request.isUrgent();
                row[6] = request.getStartTime();

                dtm.addRow(row);
            }
        }
        for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
            if (!request.isUrgent()) {
                Object[] row = new Object[7];
                row[0] = request;
                row[1] = request.getCustomer();
                row[2] = request.getPrice();
                row[3] = request.getSender();
                row[4] = request.getStatus();
                row[5] = request.isUrgent();
                row[6] = request.getStartTime();

                dtm.addRow(row);
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        btnConfirm = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        nametext = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        accounttext = new javax.swing.JTextField();

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 24)); // NOI18N
        jLabel1.setText("Front Desk Work Area");

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Order", "Customer", "Price", "Service", "Status", "Urgent"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        btnConfirm.setText("Confirm Order");
        btnConfirm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnConfirmActionPerformed(evt);
            }
        });

        jButton2.setText("Refresh");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setText("Name:");

        nametext.setEnabled(false);

        jLabel3.setText("UserAccount:");

        accounttext.setEnabled(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnConfirm)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(30, 30, 30)
                            .addComponent(nametext, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(61, 61, 61)
                            .addComponent(jLabel3)
                            .addGap(39, 39, 39)
                            .addComponent(accounttext, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton2))
                        .addComponent(jLabel1)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 663, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(131, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addComponent(jLabel1)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(nametext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(accounttext, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton2))
                .addGap(59, 59, 59)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(62, 62, 62)
                .addComponent(btnConfirm)
                .addContainerGap(67, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnConfirmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnConfirmActionPerformed
        // TODO add your handling code here:
        int selectedRow = jTable1.getSelectedRow();

        if (selectedRow < 0) {

            JOptionPane.showMessageDialog(null, "please select any row!");
            return;
        }

        OrderRequest request = (OrderRequest) jTable1.getValueAt(selectedRow, 0);

        request.setStatus("Confirm");
        request.setReceiver(userAccount);

        populate();

        this.orderRequest = request;

        /* --------------------THREAD------------------------*/
        RequestThread tt = new RequestThread();
        Thread t = new Thread(tt);
        t.start();
        /*---------------------------------------------------*/

        Organization org = null;
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (enterprise.getEnterpriseType().equals("Cinema") && organization instanceof TicketCheckOrganization) {
                org = organization;
                System.out.println(org);
                break;
            }
        }
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (enterprise.getEnterpriseType().equals("Restaurant") && organization instanceof KitchenOrganization) {
                org = organization;
                System.out.println(org);
                break;
            }
        }
        for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
            if (enterprise.getEnterpriseType().equals("Hotel") && organization instanceof CleanOrganization) {
                org = organization;
                System.out.println(org);
                break;
            }
        }
        if (org != null) {

            org.getWorkQueue().getWorkRequestList().add(request);
            organization.getWorkQueue().getWorkRequestList().add(request);
        }
    }//GEN-LAST:event_btnConfirmActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        try {
            // TODO add your handling code here:

            Socket client = null;

            client = new Socket("172.20.10.12", 4243);

            System.out.println("连接已建立...");

            InputStreamReader streamReader = new InputStreamReader(client.getInputStream());
            ObjectInputStream in = new ObjectInputStream(client.getInputStream());

            OrderRequest newRequest = (OrderRequest) in.readObject();

            System.out.println("++++" + newRequest.getPrice());

            if (newRequest.getEnterprise().getName().equals(this.enterprise.getName()) && newRequest.getStatus().equals("customer order")) {
//                for (WorkRequest request : organization.getWorkQueue().getWorkRequestList()) {
//                    if (request.getStartTime().equals(newRequest.getStartTime())) {
//                        populate();
//                        return;
//                    }
                    organization.getWorkQueue().getWorkRequestList().add(newRequest);
                
            }

            in.close();

        } catch (IOException ex) {
            Logger.getLogger(FrontDeskWorkAreaJPanel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(FrontDeskWorkAreaJPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

        populate();

    }//GEN-LAST:event_jButton2ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField accounttext;
    private javax.swing.JButton btnConfirm;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JTextField nametext;
    // End of variables declaration//GEN-END:variables
}
